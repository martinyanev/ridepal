-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema playlist_generator
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema playlist_generator
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `playlist_generator` DEFAULT CHARACTER SET utf8 ;
USE `playlist_generator` ;

-- -----------------------------------------------------
-- Table `playlist_generator`.`album`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`album` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `album_id` INT(11) NOT NULL,
  `title` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `picture` VARCHAR(255) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_generator`.`artist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`artist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `artist_id` INT(11) NOT NULL,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `picture` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_generator`.`genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`genre` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `genre_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `picture` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_generator`.`playlist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`playlist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `duration` INT(11),
  `picture` VARCHAR(45) NOT NULL,
  `user_id` INT(11),
  `genre_id` INT(11),
  `isDeleted` ENUM('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_generator`.`track`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`track` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `track_id` INT(11) NOT NULL,
  `title` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `duration` INT(11) NOT NULL,
  `rank` INT(11) NOT NULL,
  `preview` VARCHAR(255) NOT NULL,
  `picture` VARCHAR(255),
  `artist_id` INT(11) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  `album_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_generator`.`track_has_playlist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`track_has_playlist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` INT(11) NOT NULL,
  `track_id` INT(11) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_generator`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `isBanned` TINYINT(3) NULL DEFAULT NULL,
  `role` ENUM('0','1') NOT NULL DEFAULT '0',
  `avatarUrl` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `playlist_generator`.`tokens`
-- -----------------------------------------------------
CREATE TABLE `playlist_generator`.`tokens` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
