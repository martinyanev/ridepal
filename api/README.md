# Ridepal

## Description

Ridepal is a JS single-page application that users can
use to create their own playlists by travel locations
and selected genres.

## Project information and requirements

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**
- Database: **MariaDB**

## Goals

The goal of the project is to develop an app that users
enjoy using.

## Log in to the Admin user
In order to use admin rights you can use these credentials to log in or create a new user
who is going to have Basic role (you can change it through the DataBase to become Admin)

- `username`: Admin
- `password`: h2af3gse

## Log in to Basic user

- `username`: kon4e2
- `password`: kon4e2

## CRUD and Endpoints

The following CRUD operations and endpoints are implemented:

### - **GET PLAYLIST** `GET: /playlists` - returns created playlist by given queries `genres`, `percentage`, `name`, `address1`, `address2`
### - **GET PLAYLIST NAME CHECK** `GET: /playlists/check` - returns boolean if name is available for playlist (query `name`)
### - **GET PLAYLIST BY NAME** `GET: /playlists/name` - returns array of the tracks in a playlist by its name (query `playlistName`) 
### - **GET ALL TRACKS** `GET: /tracks` - returns all tracks, supports pagination `limit` and `skip` (default limit = 10 and skip = 0)
### - **GET TRACKS BY DURATION** `GET: /tracks/duration` - returns all tracks in range(query parameters) - `duration1` and `duration2` (default duration1 = 0 and duration2 = 5000)
### - **GET TRACKS BY GENRE** `GET: /tracks/genre/:id` - returns all tracks by genre
### - **GET PLAYLISTS BY USER** `GET: /playlists/user/:id` - returns all playlists for specific user
### - **GET DURATION OF TRAVEL** `GET: /travels` - returns the duration of travelling between `address1` and `address2` (query parameters)
### - **GET ALL ALBUMS** `GET: /albums` - returns all albums, supports pagination `limit` and `skip` (default limit = 10 and skip = 0)
### - **GET ALL ARTISTS** `GET: /artists` - returns all artists, supports pagination `limit` and `skip` (default limit = 10 and skip = 0)
### - **GET ALL GENRES** `GET: /genres` - returns all genres, supports pagination `limit` and `skip` (default limit = 10 and skip = 0)
### - **GET ALL USERS** - `GET: /users` - returns all users
### - **GET USER BY ID** - `GET: /users/:id` - returns user by id
### - **USER REGISTER** `POST: /users/register` - registers a user 
  - body: `{ username: String, password: String, email: String, isBanned: Boolean, role: Boolean (0 - Basic, 1 - Admin) }`
### - **USER LOGIN** `POST: /users/login` - logs in a user 
  - body: `{ username: String, password: String }`
### - **USER LOGOUT** `POST: /users/logout` - logs out a user
### - **PROMOTE ADMIN** `POST: /users/:id` - promotes user to admin
### - **DEMOTE TO BASIC USER** `PATCH: /users/:id` - demotes user to basic
### - **SET USER AVATAR** `PUT: /users/avatar` - sets user's avatar
### - **GET FAVOURITES BY GENRE** `PUT: /tracks/genre/:id` - returns certain amount of favorites by genre
### - **BAN USER** `DELETE: /users/:id` - bans a user by id
### - **UNBAN USER** `PUT: /users/:id` - unbans a user by id
### - **DELETE PLAYLIST** `DELETE: /playlists/:id` - deletes playlist by id

<br>

# Installation

### 1. Open the <span style="color: yellow"> ridepal project api</span> folder in Visual Studio Code or any other tool.
  <br>![](images_start/openFolder.png)

### 2. Open the <span style="color: yellow"> ridepal project api</span> in Integrated Terminal (right click 🖱️ on api)
  <br>![](images_start/openTerminal.png)

### 3. Run <span style="color: yellow"> npm i </span> in the Terminal to install the node modules needed to run the project.
  <br>![](images_start/runNpmInstall.png)
  <br>![](images_start/finishNpmInstall.png)

### 4. Create <span style="color: yellow"> .env </span> file inside the project folder and fill it with information like in the picture 
  <br>(Note: You can change the custom ones for your preference)
  <br>![](images_start/createEnv.png)

### 5. Download <span style="color: yellow"> MariaDB </span>
  <br>![](images_start/downloadMariaDB.png)
  <br>![](images_start/downloadMariaDB-part2.png)  

### 6. Download <span style="color: yellow"> MySQLWorkBench </span>
  <br>![](images_start/downloadMySQLWorkbench-part1.png)
  <br>![](images_start/downloadMySQLWorkbench-part2.png)
  <br>![](images_start/downloadMySQLWorkbench-part3.png)
  <br>![](images_start/downloadMySQLWorkbench-part4.png)

### 7. Now open MySQLWorkbench to set up the data base

  <br>![](images_start/downloadMySQLWorkbench-part5.png)
  <br>![](images_start/createSchema-part1.png)

### 8. Open <span style="color: yellow"> playlist_generator.sql </span> and copy the whole file code, place it in the Workbench and execute it ⚡.

  <br>![](images_start/createSchema-part2.png)
  <br>![](images_start/createSchema-part3.png)

### 9. In the opened Terminal run <span style="color: yellow"> npm run populate </span> to fill the database.(It takes around 5 mins). After it's ready you can stop it with `ctrl` + `c`.

  <br>![](images_start/npmRunPopulate-part1.png)
  <br>![](images_start/npmRunPopulate-part2.png)

### 10. Right click 🖱️ on the <span style="color: yellow"> track </span> table to see the initial data for tracks 💿

  <br>![](images_start/filledSchema-part1.png)
  <br>![](images_start/filledSchema-part2.png)
  <br>![](images_start/filledSchema-part3.png)

### 11. Now download <span style="color: yellow"> Postman </span>

   <br> ![](images_start/downloadPostman-part1.png)
   <br> ![](images_start/downloadPostman-part2.png)

### 12. Open <span style="color: yellow"> Postman </span>

   <br> ![](images_start/setUpPostman-part1.png)
   <br> ![](images_start/setUpPostman-part2.png)
   <br> ![](images_start/setUpPostman-part3.png)

### 13. Start the server by <span style="color: yellow"> npm start </span> in the Integrated Terminal (you can stop it with `ctrl` + `c`).

   <br> ![](images_start/startServer.png)
   <br> ![](images_start/startedServer.png)

### 14. Now open <span style="color: yellow"> Postman </span>  and start sending requests with the valid endpoints 🎉🎉🎉

  <br> ![](images_start/validRequest.png)

# Project resources and data format

The API exposes the following resources and sub-resources

## 1. Album

1. `id` - auto-increasing album identifying number (INT)
2. `album_id` - the album id from Deezer API (INT)
3. `title` - the name of the album (string with length in the range [1 - 255])
4. `picture` - picture of the album (string with length in the range [1 - 255])
5. `genre_id` - the id of the genre in Deezer API (INT)

## 2. Artist

1. `id` - auto-increasing artist identifying number (INT)
2. `artist_id` - the artist id from Deezer API (INT)
3. `name` - the name of the artist (string with length in the range [1 - 255])
4. `picture` - picture of the artist (string with length in the range [1 - 255])

## 3. Genre

1. `id` - auto-increasing genre identifying number (INT)
2. `genre_id` - the genre id from Deezer API (INT)
3. `name` - the name of the genre (string with length in the range [1 - 45])
4. `picture` - picture of the genre (string with length in the range [1 - 255])

## 4. Playlist

1. `id` - auto-increasing playlist identifying number (INT)
2. `title` - the name of the playlist (string with length in the range [1 - 45])
3. `duration` - the duration of the playlist in seconds (INT)
4. `picture` - picture of the playlist (string with length in the range [1 - 255])
5. `user_id` - the owner of the playlist (INT)
6. `genre_id` - the genre of the playlist (INT)
7. `isDeleted` - marks if the playlist is deleted by owner(ENUM('0','1'))

## 5. Tokens

1. `id` - auto-increasing playlist identifying number (INT)
2. `token` - Bearer token (string with length in the range [1 - 255])

## 6. Track

1. `id` - auto-increasing track identifying number (INT)
2. `track_id` - the track id from Deezer API (INT)
3. `title` - the name of the track (string with length in the range [1 - 255])
4. `duration` - the duration of the track in seconds (INT)
5. `rank` - the rank of the track in Deezer API (INT)
6. `preview` - 30 seconds of the track (string with length in the range [1 - 255])
7. `picture` - picture of the track (string with length in the range [1 - 255])
8. `artist_id` - the id of the artist in Deezer API (INT)
9. `genre_id` - the id of the genre in Deezer API (INT)
10. `album_id` - the id of the album in Deezer API (INT)

## 7. Track_has_playlist

1. `track_id` - the id of the track (INT)
2. `playlist_id` - the id of the playlist (INT)
3. `track_id` - the id of the track (INT)
4. `genre_id` - the id of the genre of the track (INT)

## 8. Users

1. `id` - auto-increasing user identifying number (number)
2. `username` - users display name (string with length in the range [1 - 45])
3. `password` - combination of numbers and symbols (string with length in the range [1 - 255])
4. `email` - email of the user (string with length in the range [1 - 45])
5. `isBanned` - marks if user is banned by admin (boolean with default NULL)
6. `role` - marks the role of the user (boolean with default 0)
7. `avatarUrl` - the url of the avatar (string with length in the range [1 - 45])

# Credits

### - Telerik Academy
<br>

# Authors

### - ©️Martin Yordanov
### - ©Martin Yanev