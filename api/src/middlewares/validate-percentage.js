
export default async (req, res, next) => {
  try {
    const { percentage, genres } = req.query;
    const arrOfGenres = genres.split(',');

    if (!percentage) {
      req.query.percentage = Array.from({length: arrOfGenres.length}, () => Math.floor(100/arrOfGenres.length)).toString()
    } else {
      const arrOfPercentage = percentage.split(',').map(Number);
    
      if (arrOfPercentage.length !== arrOfGenres.length) {
        throw new Error('genre percentage must be equal to the number of genres');
      }
      
      const sum = arrOfPercentage.reduce((acc, curr) => curr += acc , 0);
    
      if (sum > 100) {
        const number = Math.max(...arrOfPercentage);
        const index = arrOfPercentage.findIndex((el) => el === number);
        arrOfPercentage[index] -= (sum-100);
      }
      if (sum < 100) {
        const number = Math.max(...arrOfPercentage);
        const index = arrOfPercentage.findIndex((el) => el === number);
        arrOfPercentage[index] += (100-sum);
      }

      req.query.percentage = arrOfPercentage.toString();
    }

    

    await next();
  } catch (error) {
    res.status(404).json({
      error: error.message
    })
  }
}