import errorStrings from '../common/error-strings.js';
// Validates wether the genre exists

const validateGenres = ['85', '116', '152', '165', '98', '106', '129', '113'];

export default async (req, res, next) => {
  try{
      const { genres } = req.query;
      if (!genres) {
        throw new Error(errorStrings['genre']['entered']);
      }
      const arrOfGenres = genres.split(',');
      if (arrOfGenres.length === 0) {
        throw new Error(errorStrings['genre']['entered']);
      }
      if (arrOfGenres.some(el => !validateGenres.includes(el))) {
        throw new Error(errorStrings['genre']['genre']);
      }
      await next();
  } catch (error) {
      res.status(400).json({ error : error.message });
  } 
};