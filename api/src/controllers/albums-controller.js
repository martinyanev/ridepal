import express from 'express';
import songsService from '../services/songs-service.js';
import songsData from '../data/track.js';
//import validateGenre from '../middlewares/validate-genre.js';

const albumsController = express.Router();

albumsController

  // Get all albums
  
  .get('/', async (req,res) => {
    try {
      const album = 'album';
      const albums = await songsService.getAllByParam(songsData)(album, req.query);

      if (albums.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(albums);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

export default albumsController;