import express from 'express';
import songsService from '../services/songs-service.js';
import songsData from '../data/track.js';

const tracksController = express.Router();

tracksController

  // Get all tracks

  .get('/', async (req,res) => {
    try {
      const track = 'track';
      const tracks = await songsService.getAllByParam(songsData)(track, req.query);

      if (tracks.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(tracks);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

  //Get tracks by duration

  .get('/duration', async (req, res) => {
    try {
      const tracks = await songsService.getAllTracksByDuration(songsData)(req.query);

      if (tracks.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(tracks);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

  // Get Tracks by genre

  .get('/genre/:id', async (req, res) => {
    try {
      const genreId = req.params.id;
      const tracks = await songsService.getAllTracksByGenre(songsData)(genreId);

      if (tracks.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(tracks);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

  // Get all favorites by genre and rank
  
  .put('/genre/:id', async (req, res) => {
    try {
      const genreId = req.params.id;
      const tracks = await songsService.getAllFavorites(songsData)(genreId);

      if (tracks.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(tracks);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

export default tracksController;