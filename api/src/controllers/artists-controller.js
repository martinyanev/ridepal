import express from 'express';
import songsService from '../services/songs-service.js';
import songsData from '../data/track.js';
//import validateGenre from '../middlewares/validate-genre.js';

const artistsController = express.Router();

artistsController

  // Get all artists
  
  .get('/', async (req,res) => {
    try {
      const artist = 'artist';
      const artists = await songsService.getAllByParam(songsData)(artist, req.query);

      if (artists.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(artists);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

export default artistsController;