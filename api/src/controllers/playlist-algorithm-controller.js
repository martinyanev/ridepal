import express from "express";
import { authMiddleware } from "../auth/auth-middleware.js";
import playlistData from "../data/algorithm.js";
import validateGenre from "../middlewares/validate-genre.js";
import playlistAlgorithmService from "../services/playlist-algorithm-service.js";
import playlist from "../data/playlist.js";
import validatePercentage from "../middlewares/validate-percentage.js";

const playlistAlgorithmController = express.Router();

playlistAlgorithmController

  //Get playlist
  
  .get(
    "/",
    authMiddleware,
    validateGenre,
    validatePercentage,
    async (req, res) => {
      try {
        const { genres, percentage, name, address1, address2 } = req.query;
        const arrOfGenres = genres.split(",");

        const arrOfPercentage = percentage
          .split(",")
          .map((el) => Math.round((100 / el) * 10) / 10);

        const userId = req.user.id;
        const playlist = await playlistAlgorithmService.createPlaylist(
          playlistData
        )(arrOfGenres, arrOfPercentage, name, address1, address2, userId);

        if (playlist.length === 0) {
          res.status(200).json({
            msg: "no playlist",
          });
        }

        res.status(200).json(playlist);
      } catch (error) {
        res.status(400).json({
          error: error.message,
        });
      }
    }
  )

  //Get playlist name check
  
  .get("/check", authMiddleware, async (req, res) => {
    try {
      const { name } = req.query;
      const validate = await playlistAlgorithmService.checkNameOfPlaylists(
        playlist
      )(name);

      if (!validate) {
        res.status(200).json({
          msg: "invalid name",
        });
      }

      res.status(200).json(validate);
    } catch (error) {
      res.status(400).json({
        error: error.message,
      });
    }
  })

  // Get playlist by user id

  .get("/user/:id", authMiddleware, async (req, res) => {
    try {
      const userId = req.params.id;

      const playlists = await playlistAlgorithmService.getAllPlaylistsByUserId(
        playlist
      )(userId);

      res.status(200).json(playlists);
    } catch (error) {
      throw new Error(error);
    }
  })

  // Delete playlist

  .delete("/:id", authMiddleware, async (req, res) => {
    try {
      await playlistAlgorithmService.deletePlaylist(playlist)(+req.params.id);
      res.status(200).json({ message: "Playlist deleted!" });
    } catch (error) {
      res.status(400).json({
        error: error.message,
      });
    }
  })

  // Get playlist by name

  .get("/name", authMiddleware, async (req, res) => {
    try {
      const { playlistName } = req.query;
      if (!playlistName) {
        throw new Error("enter playlistName");
      }
      const playlistArr =
        await playlistAlgorithmService.getPlaylistTracksByPlaylistName(
          playlist
        )(playlistName);

      if (playlistArr.length === 0) {
        res.status(200).json({
          msg: "no playlists with such name",
        });
      }

      res.status(200).json(playlistArr);
    } catch (error) {
      res.status(400).json({
        error: error.message,
      });
    }
  });

export default playlistAlgorithmController;
