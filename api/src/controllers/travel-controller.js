import express from 'express';
import travelService from '../services/travel-service.js';
import travelData from '../data/travel.js';

const travelController = express.Router();

travelController

  //Get duration
  
  .get('/', async (req, res) => {
    try{
      const { address1, address2 } = req.query;
      const duration = await travelService.getTravelDuration(travelData)(address1, address2);

      if (duration.length === 0) {
        res.status(200).json({
          msg: "Route does not exist",
        })
      }

      res.status(200).json(duration);
    } catch(error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

export default travelController;