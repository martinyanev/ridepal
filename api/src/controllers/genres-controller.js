import express from 'express';
import songsService from '../services/songs-service.js';
import songsData from '../data/track.js';
//import validateGenre from '../middlewares/validate-genre.js';

const genresController = express.Router();

genresController

  // Get all genres
  
  .get('/', async (req,res) => {
    try {
      const genre = 'genre';
      const genres = await songsService.getAllByParam(songsData)(genre, req.query);

      if (genres.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }
      genres.pop();

      res.status(200).json(genres);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

export default genresController;