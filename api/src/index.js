import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import usersController from './controllers/users-controller.js';
import tracksController from './controllers/tracks-controller.js';
import artistsController from './controllers/artists-controller.js';
import albumsController from './controllers/albums-controller.js';
import genresController from './controllers/genres-controller.js';
import travelController from './controllers/travel-controller.js';
import playlistAlgorithmController from './controllers/playlist-algorithm-controller.js';

const config = dotenv.config().parsed;

const PORT = config.PORT;
const app = express();

app.use(express.json());
app.use(helmet());
app.use(cors());

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use('/users', usersController);
app.use('/tracks', tracksController);
app.use('/albums', albumsController);
app.use('/artists', artistsController);
app.use('/genres', genresController);
app.use('/travels', travelController);
app.use('/playlists', playlistAlgorithmController);
app.use('/app', express.static('public'));
app.use('/avatars', express.static('avatars'));

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
