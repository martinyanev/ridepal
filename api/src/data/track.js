import db from './pool.js';

const getAll = async (param, skip, limit) => {
  const sql = `
    SELECT *
    FROM ${param}
    LIMIT ?
    OFFSET ? 
  `;

  return await db.query(sql, [limit, skip]);
};

const getTrackByDuration = async (duration1, duration2) => {
  const sql = `
    SELECT *
    FROM track
    WHERE duration > ? AND duration < ?
  `;

  return await db.query(sql, [duration1, duration2]);
};

const getTrackByGenre = async (genreId) => {
  const sql = `
  SELECT t.id, t.track_id, t.title, t.duration, t.rank, t.preview, t.picture, t.artist_id, t.genre_id, t.album_id, a.name
  FROM track t
  INNER JOIN artist a
      ON t.artist_id = a.artist_id
  WHERE t.genre_id = ?
  `;

  return await db.query(sql, [genreId]);
};

const getFavorites = async (genreId) => {
  const sql = `
  SELECT t.id, t.title, t.duration, t.rank, t.preview, t.picture, t.genre_id, a.name
  FROM track t
  INNER JOIN artist a
      ON t.artist_id = a.artist_id
  WHERE t.genre_id = ?
  ORDER BY t.rank ASC
  LIMIT 16
  `;

  return await db.query(sql, [genreId]);
};

export default {
  getAll,
  getTrackByDuration,
  getTrackByGenre,
  getFavorites
};