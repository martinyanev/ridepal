import fetch from "node-fetch";

const key = 'Aos-z8XBGaYmt0DwzZv99Ph28ekOGI2-pONO54X1zyWpYxW7_oaPlB3PkCyB0SUL';

const getTravelByCity = async (address1, address2) => {

  return await fetch(`http://dev.virtualearth.net/REST/v1/Routes/driving?wp.1=${address1}&wp.2=${address2}&key=${key}`)
    .then(res => res.json())
    .then(data => data.resourceSets[0].resources[0].travelDuration)
}

export default {
  getTravelByCity,
}