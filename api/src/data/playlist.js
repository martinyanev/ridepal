import db from './pool.js';

const createPlaylist = async (name, userId, genreId) => {
  const sql = `
    INSERT INTO playlist (title, picture, user_id, genre_id, isDeleted)
    VALUES (?, ?, ?, ?, '0');
  `;
  const pictureUrl = genreId + '.jpg';
  return await db.query(sql, [name, pictureUrl, userId, genreId]);
}

const updatePlaylist = async (duration, id) => {
  const sql = `
    UPDATE playlist 
    SET duration = ?
    WHERE id = ?
  `;

  return await db.query(sql, [duration, id]);
}

const deletePlaylist = async (id) => {
  const sql = `
    UPDATE playlist 
    SET isDeleted = '1'
    WHERE id = ?
  `;

  return await db.query(sql, [id]);
}

const getPlaylistsByUserId = async (userId) => {
  const sql = `
    SELECT *
    FROM playlist p
    WHERE user_id = ?
    AND p.isDeleted = '0'
  `;

  return await db.query(sql, [userId]);
}

const getPlaylistByName = async (name) => {
  const sql = `
    SELECT t.id, t.track_id, t.title, t.duration, t.rank, t.preview, t.picture, t.artist_id, t.genre_id, t.album_id 
    FROM playlist as p
    inner join track_has_playlist as tp on p.id = tp.playlist_id
    inner join track as t on tp.track_id = t.track_id
    WHERE p.title like '${name}';
  `;

  return await db.query(sql);
}

const populateTrackHasPlaylist = async (trackId, playlistId, genreId) => {
  const sql = `
    INSERT INTO track_has_playlist (track_id, playlist_id, genre_id)
    VALUES (?, ?, ?);
  `;

  return await db.query(sql, [trackId, playlistId, genreId]);
}

const getPlaylistId = async () => {
  const sql = `
    SELECT id
    FROM playlist
    ORDER BY id
    DESC
    LIMIT 1;
  `;

  let validate = await db.query(sql) ;
  if (validate.length === 0) {
    validate = 1;
  } else {
    validate = validate[0].id + 1;
  }
  return validate;
}

const checkName = async (name) => {
  const sql = `
    SELECT *
    FROM playlist
    WHERE title LIKE ?
  `;

  return await db.query(sql, [name]);
}

export default {
  createPlaylist,
  updatePlaylist,
  getPlaylistId,
  getPlaylistByName,
  populateTrackHasPlaylist,
  checkName,
  getPlaylistsByUserId,
  deletePlaylist
}