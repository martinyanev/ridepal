import travelData from './travel.js';
import trackData from './track.js';
import playlistData from './playlist.js';

const createPlaylistAlgorithm = async (arrOfGenres, arrOfPercentage, name, address1, address2, userId) => {

  const travelTime = await travelData.getTravelByCity(address1, address2);
  const playlistId = await playlistData.getPlaylistId();

  if (arrOfGenres.length === 1) {
    await playlistData.createPlaylist(name, userId, arrOfGenres[0]);
  } else {
    await playlistData.createPlaylist(name, userId, 0);
  }

  let WholeTimeValidator = 0;

  const playlist = [];

  await Promise.all(arrOfGenres.map(async (genreId, index) => {

    let timeValidator = 0;
    const validateRepetition = [];
    const tracks = await trackData.getTrackByGenre(genreId);

    while(timeValidator < Math.floor((travelTime-220)/arrOfPercentage[index])) {
      let trackNumber = Math.floor(Math.random()*tracks.length);
      while(validateRepetition.includes(trackNumber)) {
        trackNumber = Math.floor(Math.random()*tracks.length);
      }
      validateRepetition.push(trackNumber);
      playlist.push(tracks[trackNumber]);
      await playlistData.populateTrackHasPlaylist(tracks[trackNumber].track_id, playlistId, genreId);
      timeValidator += tracks[trackNumber].duration;
    }
    
    WholeTimeValidator += timeValidator;
    
  }))

  await playlistData.updatePlaylist(WholeTimeValidator, playlistId);

  return {
    travelTime: travelTime,
    name: name,
    playlistLength: playlist.length,
    timeValidator: WholeTimeValidator,
    playlist: playlist,
  }
}

export default {
  createPlaylistAlgorithm,
}