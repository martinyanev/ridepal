import fetch from 'node-fetch';
import { DEEZER_URL } from '../common/constants.js';
import db from './pool.js';

const genres = {
  85: 'Alternative',
  116: 'Rap',
  152: 'Rock',
  165: 'R&B',
  98: 'Classical',
  106: 'Electro',
  129: 'Jazz',
  113: 'Dance'
};

const artists = [];
const covers = [];

// Seed users

const addUsers = async () => {
  const sql = `
  INSERT INTO users (username, password, email, isBanned, role)
  VALUES ('Admin', '$2b$10$io9NINYCsUjLmwHeAk0Co.I09.Ziv0RzyYNp848LO7Oy5kXWp3I5G', 'admin1@gmail.com', '0', '1'),
         ('marto', '$10$KM0tgPIT9JfkP8tnkiVKqO3Ng1s0BMWkkQfZIK9A99eWTTILfzeNq', 'marto@abv.bg', '0', '0'),
         ('kon4e2', '$2b$10$jUJf10a7HCXnvSqcBfGYrueCAV8hFWs/hhOnWAIevF0nim2Hdfi0W', 'kon4e2@gmail.com', '0', '0'),
         ('pesho2', '$2b$10$Vui2f4cGBRxHugI5Ap1A5.uwJI/CmsddJ1B76nkKFcXdIDb2Yh9ee', 'pesho2@gmail.com', '0', '0');
  `;

  await db.query(sql);
  console.log('End of addUsers');
}

// Genre populate 

const addGenres = async () => {
  const sql = `
    INSERT INTO genre (genre_id, name, picture)
    VALUES (?, ?, ?)
  `;

  await Promise.all(Object.keys(genres).map(key => {
    const pictureUrl = key + '.jpg';
    db.query(sql, [key, genres[key], pictureUrl])
  }))
  console.log('End of addGenres');
};

// Artists populate

const addArtists = async () => {
  const sql = `
    INSERT INTO artist (artist_id,name, picture)
    VALUES (?, ?, ?)
  `;

  await Promise.all(Object.keys(genres).map((key) => {
    fetch(`${DEEZER_URL}/genre/${key}/artists`)
      .then(res => res.json())
      .then(data => {
        const arr = data.data.slice(0,10);
        arr.forEach(el => artists.push([el.id, key]));
        arr.map(el => db.query(sql,[el.id, el.name, el.picture]));
      })
  }));
  console.log('End of addArtists');
};

// Include the mix genre to the seed

const addMixGenre = async () => {
  const sql = `
    INSERT INTO genre (genre_id, name, picture)
    VALUES (0, 'Mix', '0.jpg')
  `;

  await db.query(sql);
  console.log('End of addMixGenre');
}

// Album populate

const addAlbums = async () => {
  const sql = `
    INSERT INTO album (album_id, title, picture, genre_id)
    VALUES (?, ?, ?, ?)
  `;

  await Promise.all(artists.map(artist => {
      fetch(`${DEEZER_URL}/artist/${artist[0]}/albums?limit=5`)
        .then(res => res.json())
        .then(data => {
          if (data.data) {
            data.data.forEach(el => covers.push([el.id, artist[0], el.cover_big, artist[1]]));
            data.data.map(el => db.query(sql, [el.id, el.title, el.cover_big, artist[1]]));
          }
        });
  }));
  console.log('End of addAlbums');
};

// Tracks populate

const addTracks = async () => {
  const sql = `
    INSERT INTO track (track_id, title, duration, rank, preview, picture, artist_id, genre_id, album_id)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
  `;
  
  await Promise.all(covers.map((artist, index) => {
    setTimeout(() => {
      fetch(`${DEEZER_URL}/album/${artist[0]}/tracks?limit=10`)
        .then(res => res.json())
        .then(data => {
          data.data.map(el => {
            db.query(sql, [el.id, el.title, el.duration, el.rank, el.preview, artist[2], artist[1], artist[3], artist[0]]);
          })
      })
      if (index === (covers.length - 1)) {
        console.log('End of seed!');
      }
    },1000*index);
    
  }));
}


// Seed initializer

const populate = async () => {

  console.log('Loading seed...')
  await addUsers();
  await addGenres();
  await addArtists();
  await addMixGenre();
  setTimeout(async () => {
    await addAlbums();
  },5000);
  setTimeout(async () => {
    await addTracks();
  },10000);
  return;
};

populate();
