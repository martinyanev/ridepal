const getTravelDuration = travelData => {
  return async (address1, address2) => {

    return await travelData.getTravelByCity(address1, address2);
  }
};

export default {
  getTravelDuration,
}