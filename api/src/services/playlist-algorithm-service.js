import playlist from '../data/playlist.js'

const createPlaylist = playlistData => {
  return async (arrOfGenres, arrOfPercentage, name, address1, address2, userId) => {
    const validateName = await playlist.checkName(name);

    if (validateName.length !== 0) {
      throw new Error ('invalid name');
    }

    return await playlistData.createPlaylistAlgorithm(arrOfGenres, arrOfPercentage, name, address1, address2, userId);
  }
}

const getAllPlaylistsByUserId = playlistData => {
  return async (userId) => {
    
    return await playlistData.getPlaylistsByUserId(userId);
  }
}

const checkNameOfPlaylists = playlist => {
  return async (name) => {
    const validateName = await playlist.checkName(name);

    if (validateName.length !== 0) {
      return false;
    }

    return true;
  }
}
const deletePlaylist = () => {
  return async (playlistId) => {
    await playlist.deletePlaylist(playlistId);

    return true;
  }
}

const getPlaylistTracksByPlaylistName = playlist => {
  return async (name) => {
    
    return await playlist.getPlaylistByName(name); 
  }
}

export default {
  createPlaylist,
  getAllPlaylistsByUserId,
  checkNameOfPlaylists,
  deletePlaylist,
  getPlaylistTracksByPlaylistName,
}