const minNameLength = 5;
const maxNameLength = 15;
const minPassLength = 5;
const maxPassLength = 50;

export default {

    // Username should include letters and numbers!

    username: (value) => typeof value === 'string' 
        && value.length > minNameLength 
        && value.length < maxNameLength 
        && (/\d/.test(value) && /[a-zA-Z]/.test(value)),

    // Password should include letters and numbers!

    password: (value) => typeof value === 'string' 
        && value.length > minPassLength 
        && value.length < maxPassLength
        && (/\d/.test(value) && /[a-zA-Z]/.test(value)),
    
    // Email should be of valid format!
    
    email: (value) => typeof value === 'string'
        && value.length > 0
        && (/^\S+@\S+\.\S+$/.test(value)),
};
