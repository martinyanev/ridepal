# Ridepal

## Description

Ridepal is a JS single-page application that users can
use to create their own playlists by travel locations
and selected genres.

## Project information and requirements

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **React**, **Tailwind CSS**, **Material-UI**

## Goals

The goal of the project is to develop an app that users
enjoy using.

## Log in to the Admin user
In order to use admin rights you can use these credentials to log in or create a new user
who is going to have Basic role (you can change it through the DataBase to become Admin)

- `username`: Admin
- `password`: h2af3gse

## Log in to Basic user

- `username`: kon4e2
- `password`: kon4e2

## Installation 

### 1. Open the <span style="color: yellow"> ridepal project public</span> folder in Visual Studio Code or any other tool.
  <br>![](images_start/openFolder.png)

### 2. Open the <span style="color: yellow"> ridepal project public</span> in Integrated Terminal (right click 🖱️ on api)
  <br>![](images_start/openTerminal.png)

### 3. Run <span style="color: yellow"> npm i </span> in the Terminal to install the node modules needed to run the project.
  <br>![](images_start/runNpmInstall.png)
  <br>![](images_start/finishNpmInstall.png)

### 4. Run <span style="color: yellow"> npm start </span> in the Terminal to start the public server
  <br>![](images_start/runNpmStart.png)

### 5. Enjoy the App
  <br>![](images_start/enjoyApp.png)

# Credits

### - Telerik Academy
<br>

# Authors

### - ©️Martin Yordanov
### - ©Martin Yanev