import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import logo from "../../images/logo.svg";
import { ReactComponent as GithubIcon } from "../../images/github-icon.svg";

const ContainerBase = tw.div`relative h-footer`;
const Container = tw(ContainerBase)`bg-gray-900 text-gray-100 -mx-8 -mb-8`;
const Content = tw.div`max-w-screen-xl mx-auto py-16 lg:py-16`;

const Row = tw.div`flex items-center justify-center flex-col px-8`;

const LogoContainer = tw.div`flex items-center justify-center md:justify-start`;
const LogoImg = tw.img`w-8`;
const LogoText = tw.h5`ml-2 text-2xl font-body font-black tracking-wider`;

const LinksContainer = tw.div`mt-8 font-medium flex flex-wrap justify-center items-center flex-col sm:flex-row`;
const Link = tw.a`border-b-2 border-transparent font-body hocus:text-teal-300 hocus:border-teal-300 pb-1 transition duration-300 mt-2 mx-4`;

const SocialLinksContainer = tw.div`mt-10`;
const SocialLink = styled.a`
  ${tw`cursor-pointer inline-block text-2xl text-gray-100 hover:text-teal-500 transition duration-300 mx-4`}
  svg {
    ${tw`w-10 h-10`}
  }
`;

export default () => {
  return (
    <Container>
      <Content>
        <Row>
          <LogoContainer>
            <LogoImg src={logo} />
            <LogoText>
              Ride<span tw="text-primary-500">Palm</span>
            </LogoText>
          </LogoContainer>
          <LinksContainer>
            <Link href="#">Home</Link>
            <Link href="/contact-us">Contact Us</Link>
            <Link href="/genres">Music</Link>
          </LinksContainer>
          <SocialLinksContainer>
            <SocialLink href="https://gitlab.com/martinyanev/ridepal">
              <GithubIcon />
            </SocialLink>
          </SocialLinksContainer>
        </Row>
      </Content>
    </Container>
  );
};
