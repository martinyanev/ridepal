import React, { useEffect, useState } from 'react';
import tw from 'twin.macro';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import PauseCircleOutlineIcon from '@material-ui/icons/PauseCircleOutline';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import Slider from '@material-ui/core/Slider';
import Grid from '@material-ui/core/Grid';
import VolumeDown from '@material-ui/icons/VolumeDown';
import VolumeUp from '@material-ui/icons/VolumeUp';
import Marquee from "react-fast-marquee";

const Container = tw.div`mt-5 h-48 md:h-24 grid grid-cols-1 md:grid-cols-3 box-border border  bg-transparent`;
const Buttons = tw.div`flex mx-auto w-24 `;
const PlayButton = tw.button`focus:outline-none text-primary-700
transition duration-300
hocus:text-primary-500 
active:text-primary-700`;
const SliderDiv = tw.div`w-3/4 mx-auto mt-3 md:mt-8`;

const Player = ({ songInPlayer, handleNext, handlePrev }) => {
  const [ audio , setAudio] = useState(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [volume, setVolume] = useState(20);
  
  useEffect(() => {
    const audio = new Audio(songInPlayer.preview);
    setAudio(audio);
  },[songInPlayer])

  const playAudio = () => {
    audio.play();
    audio.volume=0.1;
    setIsPlaying(true);
  }

  const pauseAudio = () => {
    audio.pause();
    audio.currentTime = 0;
    setIsPlaying(false);
  }

  const handleVolume = (value) => {
    setVolume(value);
    audio.volume=value/100;
  }

  const handleNextSong = () => {
    if (isPlaying) {
      pauseAudio();
    }
    handleNext(songInPlayer.id);
  }

  const handlePrevSong = () => {
    if (isPlaying) {
      pauseAudio();
    }
    handlePrev(songInPlayer.id);
  }

  return (
    <Container>
      <Marquee
        speed='50'
        gradient={false}
        pauseOnHover={true}
      >
        <span tw="text-primary-700 font-black text-4xl">{songInPlayer.title}</span>
      </Marquee>
      <Buttons>
        <PlayButton onClick={() => handlePrevSong()}><SkipPreviousIcon fontSize="large" /></PlayButton>

        {isPlaying 
          ? <PlayButton onClick={() => pauseAudio()}><PauseCircleOutlineIcon fontSize="large" /></PlayButton>
          : <PlayButton onClick={() => playAudio()}><PlayCircleOutlineIcon fontSize="large" /></PlayButton>
        }
        <PlayButton onClick={() => handleNextSong()}><SkipNextIcon fontSize="large" /></PlayButton>
      </Buttons>
      <SliderDiv>
        <Grid container spacing={2}>
          <Grid item>
            <VolumeDown tw="text-primary-700"/>
          </Grid>
          <Grid item xs>
            <Slider 
              aria-labelledby="continuous-slider" 
              value={volume}
              onChange={(_,value) => handleVolume(value)}
              color="default"
              />
          </Grid>
          <Grid item>
            <VolumeUp tw="text-primary-700"/>
          </Grid>
        </Grid>
    </SliderDiv>
    </Container>
  )
};

export default Player;