import React, { useState } from 'react';
import tw from "twin.macro";
import styled from "styled-components";
import { Field, Form, Formik, ErrorMessage } from "formik";
import { TextField } from "@material-ui/core";
import * as Yup from "yup";
import PopUpAlert from "components/PopUpAlert/PopUpAlert";

const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold `;
const FormContainer = tw.div`w-full flex-1 mt-8`;
const SubmitButton = styled.button`
  ${tw`mt-5 tracking-wide font-black font-body bg-primary-500 
  text-lg text-gray-100 w-1/2 mx-auto py-2 rounded-lg hover:bg-primary-900 transition-all 
  duration-300 ease-in-out flex items-center justify-center focus:shadow-outline 
  focus:outline-none active:bg-pink-700 transform motion-safe:hover:scale-110`}
  .icon {
    ${tw`w-6 h-6 -ml-2`}
  }
  .text {
    ${tw`ml-3`}
  }
`;

const Destination = ({address1, address2, setAddress1, setAddress2, setTogglePickers}) => {
  const [notify, setNotify] = useState({
    isOpen: false,
    message: "",
    type: "",
  });

  const validationSchemaAddress = Yup.object().shape({
    address1: Yup.string().min(5, "It's too short").required("Required"),
    address2: Yup.string().min(5, "It's too short").required("Required"),
  });

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setNotify({
      isOpen: false,
    });
  };

  const validate = (values) => {
    const { address1, address2 } = values;
    if (address1.length > 0 && address2.length > 0) {
      if (address1 === address2) {
        setNotify({
          isOpen: true,
          message: "Invalid address",
          type: "error"
        })
        return;
      }
      setTogglePickers(true);
      setAddress1(address1);
      setAddress2(address2);
    }
  };

  return (
    <>
      <PopUpAlert 
        notify={notify}
        handleCloseNotification={handleCloseNotification}
      />
      <Heading>Choose your <span tw="text-primary-700">travel</span> destination</Heading>
      <FormContainer>
        <Formik
          initialValues={{
            address1: address1,
            address2: address2,
          }}
          validationSchema={validationSchemaAddress}
          onSubmit={validate}
        >
          {(props) => (
            <Form noValidate autoComplete="off">
              <Field
                as={TextField}
                name="address1"
                label="From:"
                fullWidth
                error={props.errors.address1 && props.touched.address1}
                helperText={<ErrorMessage name="address1" />}
                required
              />
              <Field
                as={TextField}
                name="address2"
                label="To:"
                fullWidth
                required
                error={props.errors.address2 && props.touched.address2}
                helperText={<ErrorMessage name="address2" />}
              />
              <SubmitButton variant="contained" color="default" type="submit">
                NEXT
              </SubmitButton>
            </Form>
          )}
        </Formik>
      </FormContainer>
      </>
  );
};

export default Destination;
