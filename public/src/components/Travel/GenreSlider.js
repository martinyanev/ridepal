import { BASE_URL } from "helpers/BaseURL";
import React, { useEffect, useState } from "react";
import tw from "twin.macro";
import SingleSlider from "./SingleSlider";
import TravelPlaylist from "./TravelPlaylist";

const BackButton = tw.button`
  mt-0 tracking-wide text-red-500 text-xl focus:outline-none
`;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold text-center`;
const Grid = tw.div`mt-5 p-3 grid grid-cols-1 sm:grid-cols-2 gap-5`;
const GenerateButton = tw.button`
  mt-5 tracking-wide font-black font-body bg-primary-500 
  text-xl text-gray-100 w-3/4 mx-auto py-2 rounded-lg hover:bg-primary-900 
  transition-all duration-300 ease-in-out flex items-center justify-center 
  focus:shadow-outline focus:outline-none active:bg-pink-700 transform motion-safe:hover:scale-110
`;

const GenreSlider = ({
  address1,
  address2,
  selected,
  name,
  toggleTravelPlaylist,
  setSelected,
  setToggleGenreSlider,
  setToggleTravelPlaylist,
}) => {
  const [genreNames, setGenreNames] = useState([]);
  const [slider, setSlider] = useState(
    Array.from({ length: selected.length }, () =>
      Math.floor(100 / selected.length)
    )
  );

  useEffect(() => {
    fetch(`${BASE_URL}/genres`)
      .then((res) => res.json())
      .then((data) => {
        data
          .filter((el) => selected.includes(el.genre_id))
          .map((el) => setGenreNames((old) => [...old, el.name]));
      });
  }, [selected]);

  const backButton = () => {
    setToggleGenreSlider(false);
    setSelected([]);
  };

  const generateButton = () => {
    setToggleTravelPlaylist(true);
  };

  return (
    <>
      {toggleTravelPlaylist ? (
        <TravelPlaylist
          address1={address1}
          address2={address2}
          selected={selected}
          name={name}
          slider={slider}
        />
      ) : (
        <div>
          <BackButton onClick={() => backButton()}>⬅Back</BackButton>
          <Heading>Choose genre</Heading>
          <Grid>
            {genreNames.map((el, index) => (
              <SingleSlider
                key={index}
                name={el}
                setSlider={setSlider}
                slider={slider}
                indexS={index}
              />
            ))}
          </Grid>
          <GenerateButton onClick={() => generateButton()}>
            GENERATE PLAYLIST
          </GenerateButton>
        </div>
      )}
    </>
  );
};

export default GenreSlider;
