import React from "react";
import tw from "twin.macro";
import Slider from "@material-ui/core/Slider";

const SliderDiv = tw.div`w-48`;
const Genre = tw.h3`text-xl pt-0 `;

const SingleSlider = ({ name, setSlider, slider, indexS }) => {
  const handleChange = (indexS, value) => {
    setSlider((prev) => {
      const sliders = [...prev];
      let spread = sliders.length - 1;
      const updated = sliders.map((el, index) => {
        if (index !== indexS) {
          if (el === 0 && value - sliders[indexS] > 0) {
            spread -= 1;
            return el;
          } else {
            el -= Math.round((value - sliders[indexS]) / spread);
          }
        } else {
          el = value;
        }
        return el;
      });
      return [...updated];
    });
  };

  return (
    <SliderDiv>
      <Genre>{name}</Genre>
      <Slider
        aria-label="pretto slider"
        onChange={(_, value) => handleChange(indexS, value)}
        value={slider[indexS]}
        step={slider.length - 1}
      />
    </SliderDiv>
  );
};

export default SingleSlider;
