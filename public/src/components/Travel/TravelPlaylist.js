import React, { useEffect, useState } from 'react';
import { BASE_URL } from 'helpers/BaseURL';
import SingleTrack from './SingleTrack';
import tw from 'twin.macro';
import Player from './Player';
import { Link } from 'react-router-dom';

const Heading = tw.div`grid grid-cols-1 md:grid-cols-2 mt-0 pt-0 font-body justify-center items-center font-black text-5xl text-center min-h-8`;
const Container = tw.div`w-full  mt-0 p-0 h-2/4 overflow-y-auto`;
const TrackDiv = tw.div`grid grid-cols-1 sm:grid-cols-2  md:grid-cols-4 gap-5 overflow-y-auto h-96`;

const PrimaryAction = tw.button`
rounded-full  font-body text-xl px-8 py-2 my-10 bg-gray-100
max-w-32 mx-auto
font-bold shadow transition duration-300 
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline 
active:bg-pink-700 transform motion-safe:hover:scale-110`;

const TravelPlaylist = ({ address1, address2, selected, name, slider }) => {
  const [playlist, setPlaylist] = useState([]);
  const [nameOfPlaylist, setNameOfPlaylist] = useState('');
  const [songInPlayer, setSongInPlayer] = useState({ id: null, title: '', preview: ''});

  useEffect(() => {
    const token = localStorage.getItem("tokenFinal");
    const genres = selected.toString();

    if (!slider) {
      fetch(`${BASE_URL}/playlists?genres=${genres}&name=${name}&address1=${address1}&address2=${address2}`,{
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
        .then(res => res.json())
        .then(data => {
          setPlaylist(data.playlist);
          setNameOfPlaylist(data.name);
          setSongInPlayer({
            id: 0,
            title: data.playlist[0].title,
            preview: data.playlist[0].preview
          })
        });
    } else {
      const percentage = slider.toString();

      fetch(`${BASE_URL}/playlists?genres=${genres}&percentage=${percentage}&name=${name}&address1=${address1}&address2=${address2}`,{
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
        .then(res => res.json())
        .then(data => {
          setPlaylist(data.playlist);
          setNameOfPlaylist(data.name);
          setSongInPlayer({
            id: 0,
            title: data.playlist[0].title,
            preview: data.playlist[0].preview
          })
        });
    }
  },[])

  const handleNext = (id) => {
    if (playlist[id+1]) {
      setSongInPlayer({
        id: id+1,
        title: playlist[id+1].title,
        preview: playlist[id+1].preview
      })
    } else {
      setSongInPlayer({
        id: 0,
        title: playlist[0].title,
        preview: playlist[0].preview
      })
    }
  }

  const handlePrev = (id) => {
    if (id === 0) {
      setSongInPlayer({
        id: playlist.length-1,
        title: playlist[playlist.length-1].title,
        preview: playlist[playlist.length-1].preview
      })
    } else {
      setSongInPlayer({
        id: id-1,
        title: playlist[id-1].title,
        preview: playlist[id-1].preview
      })
    }
  }

  const handleAddSong = (id, title, preview) => {
    
    setSongInPlayer({
      id: id,
      title,
      preview
    });
  }

  return (
    <>
    <Container>
        <Heading>
          {nameOfPlaylist}
            <PrimaryAction>
          <Link to="/home">
              HOME
          </Link>
              </PrimaryAction>
        </Heading>
      <TrackDiv>
        {playlist.map((el, index) => (
          <SingleTrack
            key={el.id}
            idInArray={index}
            title={el.title}
            preview={el.preview}
            picture={el.picture}
            handleAddSong={handleAddSong}
          />
        ))}
      </TrackDiv>
      <hr />
    </Container>
    <Player songInPlayer={songInPlayer} handleNext={handleNext} handlePrev={handlePrev}/>
  </>
  )
};

export default TravelPlaylist;
