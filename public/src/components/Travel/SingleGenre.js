import React, { useState } from "react";
import tw from "twin.macro";

const SelectButton = tw.button`rounded-full focus:outline-none transition duration-300 focus:shadow-outline hocus:shadow-lg transform motion-safe:hover:scale-110`;
const UnselectButton = tw.button`rounded-full focus:outline-none`;

const SingleGenre = ({ genre, setSelected }) => {
  const [border, setBorder] = useState(false);

  const onClickSelect = () => {
    setBorder(true);
    setSelected((oldArray) => [...oldArray, genre.genre_id]);
  };

  const onClickUnSelect = () => {
    setBorder(false);
    setSelected((oldArray) => oldArray.filter((id) => id !== genre.genre_id));
  };

  return (
    <>
      {border ? (
        <UnselectButton onClick={() => onClickUnSelect()}>
          <img
            src={genre.picture}
            alt="genrePic"
            tw="h-32 w-32 border-4 border-red-500 object-fill rounded-full "
          />
        </UnselectButton>
      ) : (
        <SelectButton onClick={() => onClickSelect()}>
          <img
            src={genre.picture}
            alt="genrePic"
            tw="h-32 w-32  object-fill rounded-full "
          />
        </SelectButton>
      )}
    </>
  );
};

export default SingleGenre;
