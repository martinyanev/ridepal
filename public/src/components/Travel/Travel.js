import React, { useState } from "react";
import tw from "twin.macro";
import AnimationRevealPage from "helpers/AnimationRevealPage";
import AnimationComponent from "helpers/AnimationComponent";
import GenrePicker from "./GenrePicker";
import Destination from "./Destination";

const ContainerBase = tw.div`relative h-footer content-center`;

const Container = tw(
  ContainerBase
)`flex font-body font-black min-h-screen text-white font-medium justify-center -m-8 bg-bg bg-cover bg-center`;
const Content = tw.div`mt-24 max-w-screen-xl items-center m-0 sm:mx-20 sm:my-16 bg-gray-300 text-gray-900 shadow sm:rounded-lg flex-row justify-center flex-1`;
const MainContainer = tw.div` sm:w-9/12 sm:p-6 mt-10 min-w-full`;

const MarginTop = tw.div`mt-0 xl: mt-20`;

const Travel = () => {
  const [address1, setAddress1] = useState("");
  const [address2, setAddress2] = useState("");
  const [togglePickers, setTogglePickers] = useState(false);

  return (
    <AnimationRevealPage disabled>
      <Container>
        <MarginTop>
          <AnimationComponent>
            <Content>
              <MainContainer>
                {togglePickers ? (
                  <GenrePicker
                    address1={address1}
                    address2={address2}
                    setTogglePickers={setTogglePickers}
                  />
                ) : (
                  <Destination
                    address1={address1}
                    address2={address2}
                    setAddress1={setAddress1}
                    setAddress2={setAddress2}
                    setTogglePickers={setTogglePickers}
                  />
                )}
              </MainContainer>
            </Content>
          </AnimationComponent>
        </MarginTop>
      </Container>
    </AnimationRevealPage>
  );
};

export default Travel;
