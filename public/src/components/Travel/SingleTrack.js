import React from 'react';
import tw from 'twin.macro';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';

const Card = tw.div`relative grid grid-cols-1`;
const PictureTrack = tw.p`mx-auto rounded-full group-hover:opacity-10 transition duration-300`;
const Title = tw.p`absolute mt-5 min-w-full text-center text-transparent text-2xl group-hover:text-primary-500 w-48 transition duration-300`;

const SingleTrack = ({idInArray, title, preview, picture, handleAddSong}) => {

  return (
    <Card className="group">
      <PictureTrack>
        <img src={picture} tw="rounded-lg" width="300px" alt="track" />
      </PictureTrack>
      <Title>
        {title} 
        <br />
        <button tw='focus:outline-none' onClick={() => handleAddSong(idInArray, title, preview)}>
          <AddCircleOutlineIcon fontSize="large"/>
        </button>
      </Title>
    </Card>
  )
};

export default SingleTrack;
