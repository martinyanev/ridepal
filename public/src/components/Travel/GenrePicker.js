import React, { useEffect, useState } from "react";
import tw from "twin.macro";
import { BASE_URL } from "helpers/BaseURL";
import SingleGenre from "./SingleGenre";
import PopUpAlert from "components/PopUpAlert/PopUpAlert";
import GenreSlider from "./GenreSlider";
import TravelPlaylist from "./TravelPlaylist";
import TextField from "@material-ui/core/TextField";

const Grid = tw.div`mt-5 p-3 grid grid-cols-2 sm:grid-cols-4 gap-3 items-stretch`;
const GenerateButton = tw.button`
  mt-5 tracking-wide font-black font-body bg-primary-500 
  text-xl text-gray-100 w-1/2 mx-auto py-2 rounded-lg hover:bg-primary-900 
  transition-all duration-300 ease-in-out flex items-center justify-center 
  focus:shadow-outline focus:outline-none active:bg-pink-700 transform motion-safe:hover:scale-110
`;
const BackButton = tw.button`
  mt-0 tracking-wide text-red-500 text-xl
transition duration-300
hocus:scale-110  focus:shadow-outline 
transform motion-safe:hover:scale-110
`;
const FormContainer = tw.div`w-full flex-1 text-center`;

const GenrePicker = ({ address1, address2, setTogglePickers }) => {
  const [genres, setGenres] = useState([]);
  const [selected, setSelected] = useState([]);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: "",
    type: "",
  });
  const [toggleGenreSlider, setToggleGenreSlider] = useState(false);
  const [toggleTravelPlaylist, setToggleTravelPlaylist] = useState(false);
  const [name, setName] = useState("");
  const [validateName, setValidateName] = useState("");

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setNotify({
      isOpen: false,
    });
  };

  useEffect(() => {
    fetch(`${BASE_URL}/genres`)
      .then((res) => res.json())
      .then((data) => setGenres(data));
  }, []);

  useEffect(() => {
    const token = localStorage.getItem("tokenFinal");
    fetch(`${BASE_URL}/playlists/check?name=${name}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.msg) {
          setValidateName(false);
          return;
        }
        setValidateName(true);
      });
  }, [name]);

  const generatePlaylist = () => {
    if (name.length === 0) {
      setNotify({
        isOpen: true,
        message: "Please Select Name",
        type: "error",
      });
      return;
    }
    if (selected.length === 0) {
      setNotify({
        isOpen: true,
        message: "Please Select Genre",
        type: "error",
      });
      return;
    }
    if (!validateName) {
      setNotify({
        isOpen: true,
        message: "Name already exists",
        type: "error",
      });
      return;
    }
    if (selected.length > 1) {
      setToggleGenreSlider(true);
    }
    if (selected.length === 1) {
      setToggleTravelPlaylist(true);
    }
  };

  return (
    <>
      {toggleGenreSlider ? (
        <GenreSlider
          address1={address1}
          address2={address2}
          selected={selected}
          name={name}
          toggleTravelPlaylist={toggleTravelPlaylist}
          setSelected={setSelected}
          setToggleGenreSlider={setToggleGenreSlider}
          setToggleTravelPlaylist={setToggleTravelPlaylist}
        />
      ) : toggleTravelPlaylist ? (
        <TravelPlaylist
          address1={address1}
          address2={address2}
          selected={selected}
          name={name}
        />
      ) : (
        <div>

          <PopUpAlert
            notify={notify}
            handleCloseNotification={handleCloseNotification}
          />
          <BackButton onClick={() => setTogglePickers(false)}>⬅Back</BackButton>

          <FormContainer>
            <form noValidate autoComplete="off">
              <TextField
                label="Choose name of playlist"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </form>
          </FormContainer>

          <Grid>
            {genres.map((el) => (
              <SingleGenre key={el.id} genre={el} setSelected={setSelected} />
            ))}
          </Grid>
          <GenerateButton onClick={() => generatePlaylist()}>
            GENERATE PLAYLIST
          </GenerateButton>
        </div>
      )}
      </>
  );
};

export default GenrePicker;
