import React from "react";
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import logo from "images/logo.svg";
import AnimationComponent from "helpers/AnimationComponent";
import { TextField } from "@material-ui/core";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { BASE_URL } from "../../helpers/BaseURL";
import decode from "jwt-decode";
import AuthContext from "../../providers/authContext";
import { useState, useContext } from "react";
import PopUpAlert from "components/PopUpAlert/PopUpAlert";

const ContainerBase = tw.div`relative h-footer content-center`;

const Container = tw(
  ContainerBase
)`flex font-body font-black min-h-screen text-white font-medium justify-center -m-8 bg-bg bg-cover bg-center`;
const Content = tw.div`max-w-screen-md items-center m-0 sm:mx-20 sm:my-16 bg-gray-300 text-gray-900 
shadow sm:rounded-lg flex-row justify-center flex-1`;
const MainContainer = tw.div` lg:w-1/2 xl:w-6/12 p-6 sm:p-12 min-w-full`;
const LogoLink = tw.a``;
const LogoImage = tw.img`h-12 mx-auto`;
const MainContent = tw.div`mt-12 flex flex-col items-center `;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold `;
const FormContainer = tw.div`w-full flex-1 mt-8`;

const DividerTextContainer = tw.div`my-12 border-b text-center relative`;
const DividerText = tw.div`leading-none px-2 inline-block text-sm text-gray-600 tracking-wide 
font-medium bg-white transform -translate-y-1/2 absolute inset-x-0 top-1/2 bg-transparent`;

const SubmitButton = styled.button`
  ${tw`mt-5 tracking-wide font-black font-body bg-primary-500 text-xl text-gray-100 w-full py-4 
  rounded-lg hover:bg-primary-900 transition-all duration-300 ease-in-out flex items-center justify-center 
  focus:shadow-outline focus:outline-none active:bg-pink-700 transform motion-safe:hover:scale-110`}
  .icon {
    ${tw`w-6 h-6 -ml-2`}
  }
  .text {
    ${tw`ml-3`}
  }
`;

const Login = ({
  logoLinkUrl = "/home",
  forgotPasswordUrl = "/contact-us",
  registerToggle,
}) => {
  const passwordRegExp = /^(?=.*\d)(?=.*[a-z]).{5,}$/;

  const auth = useContext(AuthContext);

  const [user, setUser] = useState({
    username: "",
    password: "",
    email: "",
  });

  const [notify, setNotify] = useState({
    isOpen: false,
    message: "",
    type: "",
  });

  const validationSchemaLogin = Yup.object().shape({
    name: Yup.string().min(3, "It's too short").required("Required"),
    password: Yup.string()
      .min(5, "Minimum characters should be 5")
      .matches(
        passwordRegExp,
        "Password must have at least 5 characters and a number"
      )
      .required("Required"),
  });

  const onLogin = (values) => {
    fetch(`${BASE_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: values.name,
        password: values.password,
      }),
    })
      .then((r) => r.json())
      .then(({ tokenFinal }) => {
        if (!tokenFinal) {
          setNotify({
            isOpen: true,
            message: "invalid password or username",
            type: "error",
          });
          return;
        }
        const user = decode(tokenFinal);
        setUser(user);
        localStorage.setItem("tokenFinal", tokenFinal);
        auth.setAuthState({ user, isLoggedIn: true });
        window.location.href = "/home";
      })
      .catch(console.warn);
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setNotify({
      isOpen: false,
    });
  };

  return (
    <AnimationRevealPage disabled>
      <PopUpAlert
        notify={notify}
        handleCloseNotification={handleCloseNotification}
      />
      <Container>
        <AnimationComponent>
          <Content>
            <MainContainer>
              <LogoLink href={logoLinkUrl}>
                <LogoImage src={logo} />
              </LogoLink>
              <MainContent>
                <Heading>
                  Sign in to Ride<span tw="text-primary-500">Pal</span>
                </Heading>
                <FormContainer>
                  <DividerTextContainer>
                    <DividerText>Please fill out the form</DividerText>
                  </DividerTextContainer>
                  <Formik
                    initialValues={user}
                    validationSchema={validationSchemaLogin}
                    onSubmit={onLogin}
                  >
                    {(props) => (
                      <Form noValidate autocomplete="off">
                        <Field
                          as={TextField}
                          name="name"
                          label="Username"
                          fullWidth
                          error={props.errors.name && props.touched.name}
                          helperText={<ErrorMessage name="name" />}
                          required
                        />

                        <Field
                          as={TextField}
                          name="password"
                          label="Password"
                          type="password"
                          fullWidth
                          error={
                            props.errors.password && props.touched.password
                          }
                          helperText={<ErrorMessage name="password" />}
                          required
                        />
                        <SubmitButton
                          type="submit"
                          variant="contained"
                          color="default"
                        >
                          LOGIN
                        </SubmitButton>
                      </Form>
                    )}
                  </Formik>
                  <p tw="mt-6 text-xs text-gray-600 text-center">
                    <a
                      href={forgotPasswordUrl}
                      tw="border-b border-gray-500 border-dotted"
                    >
                      Forgot Password ?
                    </a>
                  </p>
                  <p tw="mt-8 text-sm text-gray-600 text-center">
                    Dont have an account?{" "}
                    <button
                      onClick={() => registerToggle()}
                      tw="border-b border-gray-500 border-dotted"
                    >
                      Sign Up
                    </button>
                  </p>
                </FormContainer>
              </MainContent>
            </MainContainer>
          </Content>
        </AnimationComponent>
      </Container>
    </AnimationRevealPage>
  );
};

export default Login;
