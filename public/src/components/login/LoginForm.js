import React from "react";
import { useState } from "react";
import Register from "./Register";
import Login from "./Login";

const LoginForm = () => {
  const [register, toggleRegister] = useState(false);

  const registerToggle = () => {
    toggleRegister(true);
  };

  return (
    <div>
      {register ? <Register /> : <Login registerToggle={registerToggle} />}
    </div>
  );
};

export default LoginForm;
