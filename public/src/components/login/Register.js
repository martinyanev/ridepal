import React from "react";
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import logo from "images/logo.svg";
import AnimationComponent from "helpers/AnimationComponent";
import { TextField } from "@material-ui/core";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { BASE_URL } from "../../helpers/BaseURL";

const ContainerBase = tw.div`relative h-footer content-center`;
const Container = tw(
  ContainerBase
)`flex font-body font-black min-h-screen text-white font-medium justify-center -m-8 bg-bg bg-cover bg-center`;
const Content = tw.div`max-w-lg items-center m-0 sm:mx-20 sm:my-16 bg-gray-300 text-gray-900 
shadow sm:rounded-lg flex-row justify-center flex-1`;
const MainContainer = tw.div` lg:w-1/2 xl:w-6/12 p-6 sm:p-12 min-w-full`;
const LogoLink = tw.a``;
const LogoImage = tw.img`h-12 mx-auto`;
const MainContent = tw.div`mt-12 flex flex-col items-center `;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold `;
const FormContainer = tw.div`w-full flex-1 mt-8`;

const SubmitButton = styled.button`
  ${tw`mt-5 tracking-wide font-black text-xl font-body bg-primary-500 text-gray-100 w-full py-4 
  rounded-lg hover:bg-primary-900 transition-all duration-300 ease-in-out flex items-center justify-center 
  focus:shadow-outline focus:outline-none active:bg-pink-700 transform motion-safe:hover:scale-110`}
  .icon {
    ${tw`w-6 h-6 -ml-2`}
  }
  .text {
    ${tw`ml-3`}
  }
`;

const DividerTextContainer = tw.div`my-12 border-b text-center relative`;
const DividerText = tw.div`leading-none px-2 inline-block text-sm text-gray-600 tracking-wide 
font-medium bg-white transform -translate-y-1/2 absolute inset-x-0 top-1/2 bg-transparent`;

const Register = ({ logoLinkUrl = "/home" }) => {
  const passwordRegExp = /^(?=.*\d)(?=.*[a-z]).{5,}$/;
  const initialValues = {
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
  };

  const validationSchema = Yup.object().shape({
    name: Yup.string().min(3, "It's too short").required("Required"),
    email: Yup.string().email("Enter valid email").required("Required"),
    password: Yup.string()
      .min(5, "Minimum characters should be 5")
      .matches(
        passwordRegExp,
        "Password must have at least 5 characters and a number"
      )
      .required("Required"),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref("password")], "Password doesn't match")
      .required("Required"),
  });

  const onRegister = (values) => {
    fetch(`${BASE_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: values.name,
        password: values.password,
        email: values.email,
      }),
    })
      .then((res) => res.json())
      .then((data) => console.log(data))
      .catch((error) => console.log(error));
    window.location.href = "/login";
  };

  return (
    <AnimationRevealPage disabled>
      <Container>
        <AnimationComponent>
          <Content>
            <MainContainer>
              <LogoLink href={logoLinkUrl}>
                <LogoImage src={logo} />
              </LogoLink>
              <MainContent>
                <Heading>
                  Register for Ride<span tw="text-primary-500">Pal</span>
                </Heading>
                <FormContainer>
                  <DividerTextContainer>
                    <DividerText>Please fill out the form</DividerText>
                  </DividerTextContainer>
                  <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={onRegister}
                  >
                    {(props) => (
                      <Form noValidate>
                        <Field
                          as={TextField}
                          name="name"
                          label="Username"
                          fullWidth
                          error={props.errors.name && props.touched.name}
                          helperText={<ErrorMessage name="name" />}
                          required
                        />

                        <Field
                          as={TextField}
                          name="email"
                          label="Email"
                          fullWidth
                          error={props.errors.email && props.touched.email}
                          helperText={<ErrorMessage name="email" />}
                          required
                        />

                        <Field
                          as={TextField}
                          name="password"
                          label="Password"
                          type="password"
                          fullWidth
                          error={
                            props.errors.password && props.touched.password
                          }
                          helperText={<ErrorMessage name="password" />}
                          required
                        />

                        <Field
                          as={TextField}
                          name="confirmPassword"
                          label="Confirm Password"
                          type="password"
                          fullWidth
                          error={
                            props.errors.confirmPassword &&
                            props.touched.confirmPassword
                          }
                          helperText={<ErrorMessage name="confirmPassword" />}
                          required
                        />

                        <SubmitButton
                          type="submit"
                          variant="contained"
                          color="orange"
                        >
                          REGISTER
                        </SubmitButton>
                      </Form>
                    )}
                  </Formik>
                </FormContainer>
              </MainContent>
            </MainContainer>
          </Content>
        </AnimationComponent>
      </Container>
    </AnimationRevealPage>
  );
};

export default Register;
