import React from "react";
import Slider from "react-slick";
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro"; //eslint-disable-line
import { ReactComponent as ArrowLeftIcon } from "../../images/arrow-left-2-icon.svg";
import { ReactComponent as ArrowRightIcon } from "../../images/arrow-right-2-icon.svg";
import { ReactComponent as SvgDecoratorBlob1 } from "../../images/svg-decorator-blob-4.svg";
import { ReactComponent as SvgDecoratorBlob2 } from "../../images/svg-decorator-blob-5.svg";
import { ReactComponent as SvgDecoratorBlob3 } from "../../images/svg-decorator-blob-3.svg";
import { ReactComponent as SvgDecoratorBlob4 } from "../../images/svg-decorator-blob-1.svg";
import { ReactComponent as SvgDecoratorBlob5 } from "../../images/dot-pattern.svg";

import "slick-carousel/slick/slick.css";

const HeadingTitle = tw.h2`text-2xl sm:text-6xl font-body tracking-wide text-center`;
const Container = tw.div`relative bg-white`;
const Content = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24 static`;
const HeadingInfoContainer = tw.div`flex flex-col items-center`;
const HeadingDescription = tw.p`mt-4 font-medium text-gray-600 text-center max-w-sm`;

const SliderContainer = tw.div`m-5`;
const AboutUsSlider = styled(Slider)``;
const AboutUsInfo = tw.div`flex! flex-row items-center md:items-center md:flex-row md:justify-center`;
const ImageContainer = styled.div`
  ${tw`md:mx-3 lg:mx-6 w-2/3 md:w-4/12 rounded flex items-center max-w-xs md:max-w-none max-h-team `}
  img {
    ${tw` min-h-team min-w-team items-stretch `}
  }
`;
const TextContainer = tw.div`md:mx-3 lg:mx-6 md:w-6/12 py-4 flex flex-col justify-between`;
const AboutContainer = tw.div`relative p-6 md:p-8 lg:p-10 mt-4 md:mt-0 `;
const About = tw.blockquote`text-center md:text-left font-body font-black text-4xl lg:text-4xl xl:text-3xl`;
const Info = tw.div`px-5 lg:px-10 text-center md:text-left mt-4 md:mt-0`;
const Name = tw.h5`font-bold text-lg lg:text-xl xl:text-2xl text-primary-500`;
const Title = tw.p`font-medium text-sm`;
const SliderControlButtonContainer = styled.div`
  ${tw`absolute top-0 h-full flex items-end md:items-center z-20`}
  button {
    ${tw`text-secondary-500 hover:text-primary-500 focus:outline-none 
    transition duration-300 transform hover:scale-125 transform -translate-y-2/3 md:translate-y-0`}
    svg {
      ${tw`w-8`}
    }
  }
`;
const DecoratorBlob = styled(SvgDecoratorBlob3)`
  ${tw` pointer-events-none absolute right-0 bottom-0 w-64 opacity-25 transform translate-x-32 translate-y-48`}
`;
const DecoratorBlob1 = styled(SvgDecoratorBlob1)`
  ${tw` pointer-events-none absolute right-0 bottom-0 w-64 opacity-25 transform translate-x-32 translate-y-48`}
`;

const DecoratorBlob2 = styled(SvgDecoratorBlob2)`
  ${tw`pointer-events-none absolute left-0 top-0 w-1/12 opacity-75 transform translate-x-0 translate-y-48 `}
`;
const DecoratorBlob3 = styled(SvgDecoratorBlob3)`
  ${tw`pointer-events-none absolute top-0 right-0 w-64 opacity-25 transform translate-x-32 translate-y-48 `}
`;
const DecoratorBlob4 = styled(SvgDecoratorBlob4)`
  ${tw`pointer-events-none absolute top-0 left-0 w-64 opacity-25 transform translate-x-0 translate-y-48 `}
`;
const DecoratorBlob5 = styled(SvgDecoratorBlob5)`
  ${tw`pointer-events-none absolute top-0 left-0 w-64 opacity-25 transform translate-x-24 translate-y-48 bg-opacity-25 `}
`;

const NextArrow = ({ currentSlide, slideCount, ...props }) => (
  <SliderControlButtonContainer tw="right-0">
    <button {...props}>
      <ArrowRightIcon />
    </button>
  </SliderControlButtonContainer>
);
const PreviousArrow = ({ currentSlide, slideCount, ...props }) => (
  <SliderControlButtonContainer tw="left-0">
    <button {...props}>
      <ArrowLeftIcon />
    </button>
  </SliderControlButtonContainer>
);

export default () => {
  const team = [
    {
      imageSrc: "yanev.jpg",
      quote: "23 year old duck tycoon",
      name: "Martin Yanev",
      city: "Varna, Bulgaria",
    },
    {
      imageSrc: "bobthebuilder.jpg",
      quote: "21 year old Bob the Builder",
      name: "Martin Yordanov",
      city: "Sofia, Bulgaria",
    },
  ];
  return (
    <Container>
      <Content>
        <HeadingInfoContainer>
          <HeadingTitle>
            The <span tw="text-primary-500">Team</span>
          </HeadingTitle>
          <HeadingDescription></HeadingDescription>
        </HeadingInfoContainer>
        <SliderContainer>
          <AboutUsSlider
            nextArrow={<NextArrow />}
            prevArrow={<PreviousArrow />}
          >
            {team.map((martin, index) => (
              <AboutUsInfo key={index}>
                <ImageContainer>
                  <img src={martin.imageSrc} alt={martin.name} />
                </ImageContainer>
                <TextContainer>
                  <AboutContainer>
                    <About>{martin.quote}</About>
                  </AboutContainer>
                  <Info>
                    <Name>{martin.name}</Name>
                    <Title>{martin.city}</Title>
                  </Info>
                </TextContainer>
              </AboutUsInfo>
            ))}
          </AboutUsSlider>
        </SliderContainer>
        <DecoratorBlob />
        <DecoratorBlob1 />
        <DecoratorBlob2 />
        <DecoratorBlob3 />
        <DecoratorBlob4 />
        <DecoratorBlob5 />
      </Content>
    </Container>
  );
};
