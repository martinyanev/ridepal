import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import {
  SectionHeading,
  Subheading as SubheadingBase,
} from "components/misc/Headings.js";
import EmailIllustrationSrc from "images/email-illustration.svg";
import AnimationRevealPage from "helpers/AnimationRevealPage";
import emailjs from "emailjs-com";

const Container = tw.div`relative `;
const TwoColumn = tw.div`flex flex-col md:flex-row justify-center max-w-screen-xl mx-auto mt-16`;
const Column = tw.div`w-full max-w-md mx-auto md:max-w-none md:mx-0`;
const ImageColumn = tw(Column)`md:w-5/12 flex-shrink-0 h-80 md:h-auto`;
const TextColumn = styled(Column)((props) => [
  tw`md:w-7/12 mt-16 md:mt-0 `,
  props.textOnLeft
    ? tw`md:mr-12 lg:mr-16 md:order-first`
    : tw`md:ml-12 lg:ml-16 md:order-last`,
]);

const Image = styled.div((props) => [
  `background-image: url("${props.imageSrc}");`,
  tw`rounded bg-contain bg-no-repeat bg-center h-full`,
]);
const TextContent = tw.div`lg:py-8 text-center md:text-left`;

const Subheading = tw(SubheadingBase)`text-center font-body md:text-left`;
const Heading = tw(
  SectionHeading
)` font-black text-left text-6xl sm:text-4xl lg:text-5xl font-body text-center md:text-left leading-tight`;
const Description = tw.p`mt-4 text-center md:text-left 
text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100`;

const Form = tw.form` md:mt-10 text-lg flex flex-col max-w-sm mx-auto md:mx-0`;
const Input = tw.input` first:mt-0 border-b-2 py-3 focus:outline-none 
font-medium transition duration-300 hocus:border-primary-500`;
const Textarea = styled(Input).attrs({ as: "textarea" })`
  ${tw`h-24`}
`;

const PrimaryAction = tw.button`
rounded-full  font-body text-xl px-8 py-3 my-10 bg-gray-100 
font-bold shadow transition duration-300 
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline 
active:bg-pink-700 transform motion-safe:hover:scale-110`;

export default ({
  subheading = "Contact Us",
  heading = (
    <>
      Feel free to <span tw="text-primary-500">get in touch</span>
      <wbr /> with us.
    </>
  ),
  description = "Feel free to contact us if you have any questions or inquiries.",
  submitButtonText = "Send",
  textOnLeft = true,
}) => {
  function sendEmail(e) {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_s37wk29",
        "template_wnogg0s",
        e.target,
        "user_iwdUUJCgMKNoKbS3jsNLs"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      )
      .then((res) => (window.location.href = "/home"));
  }

  return (
    <AnimationRevealPage>
      <Container>
        <TwoColumn>
          <ImageColumn>
            <Image imageSrc={EmailIllustrationSrc} />
          </ImageColumn>
          <TextColumn textOnLeft={textOnLeft}>
            <TextContent>
              {subheading && <Subheading>{subheading}</Subheading>}
              <Heading>{heading}</Heading>
              {description && <Description>{description}</Description>}
              <Form onSubmit={sendEmail}>
                <Input
                  type="email"
                  name="email"
                  placeholder="Your Email Address"
                />
                <Input type="text" name="name" placeholder="Full Name" />
                <Input type="text" name="subject" placeholder="Subject" />
                <Textarea name="message" placeholder="Your Message Here" />
                <PrimaryAction type="submit">{submitButtonText}</PrimaryAction>
              </Form>
            </TextContent>
          </TextColumn>
        </TwoColumn>
      </Container>
    </AnimationRevealPage>
  );
};
