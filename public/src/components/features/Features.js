import React from "react";
import styled from "styled-components";
import tw from "twin.macro";
//eslint-disable-next-line
import { css } from "styled-components/macro";
import { ReactComponent as SvgDecoratorBlob1 } from "../../images/svg-decorator-blob-1.svg";
import { ReactComponent as SvgDecoratorBlob2 } from "../../images/svg-decorator-blob-2.svg";
import { ReactComponent as SvgDecoratorBlob3 } from "../../images/svg-decorator-blob-3.svg";
import { ReactComponent as SvgDecoratorBlob5 } from "../../images/svg-decorator-blob-5.svg";
import SupportIconImage from "../../images/support-icon.svg";
import CustomizeIconImage from "../../images/customize-icon.svg";
import SimpleIconImage from "../../images/simple-icon.svg";
import { Link } from "react-router-dom";

export const SectionHeading = tw.h2`text-4xl sm:text-5xl font-black tracking-wide text-center`;

const Container = tw.div`relative`;

const ThreeColumnContainer = styled.div`
  ${tw`flex flex-col items-center md:items-stretch md:flex-row flex-wrap 
  md:justify-center max-w-screen-xl mx-auto py-20 md:py-24`}
`;
const Heading = tw(SectionHeading)`w-full`;

const Column = styled.div`
  ${tw`md:w-1/2 lg:w-1/3 px-6 flex`}
`;

const Card = styled.div`
  ${tw`flex flex-col mx-auto max-w-xs items-center px-6 py-10 border-2 border-dashed 
  border-primary-500 rounded-lg mt-12 transition duration-300 
  bg-white text-gray-100 hocus:bg-primary-100 
  hocus:text-gray-200 focus:outline-none focus:shadow-outline 
  active:bg-primary-700 transform motion-safe:hover:scale-110`}
  .imageContainer {
    ${tw`border-2 border-primary-500 text-center rounded-full p-6 flex-shrink-0 relative`}
    img {
      ${tw`w-8 h-8`}
    }
  }

  .textContainer {
    ${tw`mt-6 text-center font-body`}
  }

  .title {
    ${tw`mt-2 font-bold text-2xl leading-none text-primary-500`}
  }

  .description {
    ${tw`mt-5 font-black text-secondary-100 text-lg leading-loose`}
  }
`;

const DecoratorBlob = styled(SvgDecoratorBlob3)`
  ${tw`pointer-events-none absolute right-0 bottom-0 w-64 opacity-25 transform translate-x-32 translate-y-48 `}
`;
const DecoratorBlob2 = styled(SvgDecoratorBlob2)`
  ${tw`pointer-events-none absolute left-0 top-0 w-1/12 opacity-75 transform translate-x-0 translate-y-48 `}
`;
const DecoratorBlob3 = styled(SvgDecoratorBlob5)`
  ${tw`pointer-events-none absolute top-0 right-0 w-64 opacity-25 transform translate-x-32 translate-y-48 `}
`;
const DecoratorBlob6 = styled(SvgDecoratorBlob1)`
  ${tw`pointer-events-none absolute bottom-1/2 left-0 w-64 opacity-25 transform translate-x-0 translate-y-64 `}
`;

export default () => {
  const cards = [
    {
      imageSrc: SupportIconImage,
      title: "Music Collection",
      description: "Browse through our various artists, albums and tracks.",
      href: "/genres",
    },
    {
      imageSrc: SimpleIconImage,
      title: "User Friendly",
      description:
        "Everything is made with the idea of being simple and easy to use!",
      href: "/profile",
    },
    {
      imageSrc: CustomizeIconImage,
      title: "Customizable",
      description:
        "Create your own playlist, based on preferences and travel time!",
      href: "/travel",
    },
  ];

  return (
    <Container>
      <ThreeColumnContainer>
        <Heading tw="font-body">
          Our <span tw="text-primary-500">Services</span>
        </Heading>
        {cards.map((card, i) => (
          <Column key={i}>
            <Link to={`${card.href}`}>
              <Card>
                <span className="imageContainer">
                  <img src={card.imageSrc} alt="cardImage" />
                </span>
                <span className="textContainer">
                  <span className="title">{card.title}</span>
                  <p className="description">{card.description}</p>
                </span>
              </Card>
            </Link>
          </Column>
        ))}
      </ThreeColumnContainer>
      <DecoratorBlob />
      <DecoratorBlob2 />
      <DecoratorBlob3 />
      <DecoratorBlob6 />
    </Container>
  );
};
