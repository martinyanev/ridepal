import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import { Container, ContentWithPaddingXl } from "components/misc/Layouts.js";
import { SectionHeading } from "components/misc/Headings.js";
import { ReactComponent as SvgDecoratorBlob1 } from "images/svg-decorator-blob-5.svg";
import { ReactComponent as SvgDecoratorBlob2 } from "images/svg-decorator-blob-7.svg";
import { ReactComponent as SvgDecoratorBlob3 } from "images/svg-decorator-blob-3.svg";
import { ReactComponent as SvgDecoratorBlob4 } from "images/svg-decorator-blob-2.svg";
import { ReactComponent as SvgDecoratorBlob5 } from "images/svg-decorator-blob-1.svg";
import { ReactComponent as SvgDecoratorBlob6 } from "images/svg-decorator-blob-4.svg";
import { ReactComponent as SvgDecoratorBlob7 } from "images/svg-decorator-blob-6.svg";
import { ReactComponent as ClockIcon } from "feather-icons/dist/icons/clock.svg";
import { ReactComponent as PlayIcon } from "feather-icons/dist/icons/play.svg";
import { ReactComponent as PauseIcon } from "feather-icons/dist/icons/pause.svg";

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const Header = tw(SectionHeading)`font-body font-black text-5xl`;
const TabsControl = tw.div`flex flex-wrap bg-gray-200 px-2 font-body py-2 rounded leading-none mt-12 xl:mt-0`;

const TabControl = styled.div`
  ${tw`cursor-pointer px-6 py-3 mt-2 sm:mt-0 sm:mr-2 last:mr-0 text-gray-600 
  font-medium rounded-sm transition duration-300 text-sm sm:text-base w-1/2 sm:w-auto text-center`}
  &:hover {
    ${tw`bg-gray-300 text-gray-700`}
  }
  ${(props) => props.active && tw`bg-primary-500! text-gray-100!`}
  }
`;

const TabContent = tw(
  motion.div
)`mt-6 flex flex-wrap sm:-mr-10 md:-mr-6 lg:-mr-12`;
const CardContainer = tw.div`mt-10 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 sm:pr-10 md:pr-6 lg:pr-12`;
const Card = tw(
  motion.a
)`bg-gray-200 rounded-b block max-w-xs mx-auto min-h-songs sm:max-w-none sm:mx-0`;
const CardImageContainer = styled.div`
  ${(props) =>
    css`
      background-image: url("${props.imageSrc}");
    `}
  ${tw`h-56 xl:h-64 bg-center bg-cover relative rounded-t`}
`;
const CardDuration = styled.div`
  ${tw`mr-1 text-xl font-bold flex items-end`}
  svg {
    ${tw`w-8 h-8 text-primary-400 mr-1`}
  }
`;

const CardHoverOverlay = styled(motion.div)`
  background-color: rgba(255, 255, 255, 0.5);
  ${tw`absolute inset-0 flex justify-center items-center`}
`;

const CardText = tw.div`p-4 text-gray-900 font-display font-black`;
const CardTitle = tw.h5`text-xl font-black group-hover:text-primary-500`;
const CardContent = tw.p`mt-1 text-sm font-medium text-gray-600`;
const CardDurationContainer = tw.p`mt-4 text-xl font-black font-body`;

const DecoratorBlob1 = styled(SvgDecoratorBlob1)`
  ${tw`pointer-events-none -z-20 absolute right-0 top-0 h-64 w-64 
  opacity-15 transform translate-x-1/2 -translate-y-12 text-pink-400`}
`;
const DecoratorBlob2 = styled(SvgDecoratorBlob2)`
  ${tw`pointer-events-none -z-20 absolute left-0 bottom-0 h-80 w-80 
  opacity-15 transform -translate-x-2/3 text-primary-500`}
`;
const DecoratorBlob3 = styled(SvgDecoratorBlob3)`
  ${tw`pointer-events-none -z-20 absolute left-0 bottom-0 h-80 w-80 
  opacity-15 transform -translate-x-1/2  text-primary-500`}
`;
const DecoratorBlob4 = styled(SvgDecoratorBlob4)`
  ${tw`pointer-events-none -z-20 absolute right-0 bottom-0 top-0 h-80 w-80 
  opacity-15 transform -translate-x-1/2  text-primary-500`}
`;
const DecoratorBlob5 = styled(SvgDecoratorBlob5)`
  ${tw`pointer-events-none -z-20 absolute top-0 bottom-0 h-80 w-80 
  opacity-15 transform -translate-x-2/3 text-primary-500`}
`;
const DecoratorBlob6 = styled(SvgDecoratorBlob6)`
  ${tw`pointer-events-none -z-20 absolute right-0 bottom-0 h-80 w-80 
  opacity-15 transform -translate-x-2/3 text-primary-500`}
`;
const DecoratorBlob7 = styled(SvgDecoratorBlob7)`
  ${tw`pointer-events-none -z-20 absolute left-auto bottom-0 h-80 w-80 
  opacity-15 transform -translate-x-2/3 text-primary-500`}
`;
const DecoratorBlob8 = styled(SvgDecoratorBlob7)`
  ${tw`pointer-events-none -z-20 absolute left-1/2 bottom-0 h-80 w-80 
  opacity-15 transform -translate-x-2/3 text-primary-500`}
`;

const PrimaryAction = tw.button`
rounded-full  font-body text-xl px-5 py-5 my-10 bg-gray-100 
font-bold shadow transition duration-300 
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline 
active:bg-pink-700 transform motion-safe:hover:scale-110`;

export default () => {
  const genres = {
    Alternative: 85,
    Rap: 116,
    Rock: 152,
    "R&B": 165,
    Classical: 98,
    Electro: 106,
    Jazz: 129,
  };

  const [tracks, setTracks] = useState([]);

  const tabs = {
    Rock: tracks,
    "R&B": tracks,
    Rap: tracks,
    Jazz: tracks,
    Alternative: tracks,
    Classical: tracks,
    Electro: tracks,
  };
  const tabsKeys = Object.keys(tabs);
  const [activeTab, setActiveTab] = useState(tabsKeys[0]);

  useEffect(() => {
    fetch(`http://localhost:5555/tracks/genre/${genres[activeTab]}`, {
      method: "PUT",
    })
      .then((res) => res.json())
      .then((data) => setTracks(data.sort(() => Math.random() - 0.5)))
  }, [activeTab]);

  const [currentSong, setCurrentSong] = useState(tracks[0]);
  const [isPlaying, setIsPlaying] = useState(false);

  const soundPlay = (src) => {
    const sound = new Audio(src);
    setIsPlaying(true);
    setTracks(tracks);
    sound.play();
    sound.volume = 0.1;
    setCurrentSong(sound);
  };

  const soundPause = () => {
    setIsPlaying(false);
    currentSong.pause();
    currentSong.currentTime = 0;
  };

  return (
    <Container>
      <ContentWithPaddingXl>
        <HeaderRow>
          <Header>
            Featured <span tw="text-primary-600">songs</span>
          </Header>
          <TabsControl>
            {Object.keys(tabs).map((tabName, index) => (
              <TabControl
                key={index}
                active={activeTab === tabName}
                onClick={() => setActiveTab(tabName)}
              >
                {tabName}
              </TabControl>
            ))}
          </TabsControl>
        </HeaderRow>

        {tabsKeys.map((tabKey, index) => (
          <TabContent
            key={index}
            variants={{
              current: {
                opacity: 1,
                scale: 1,
                display: "flex",
              },
              hidden: {
                opacity: 0,
                scale: 0.3,
                display: "none",
              },
            }}
            transition={{ duration: 0.7 }}
            initial={activeTab === tabKey ? "current" : "hidden"}
            animate={activeTab === tabKey ? "current" : "hidden"}
          >
            {tracks.map((card, index) => (
              <CardContainer key={index}>
                <Card
                  className="group"
                  href={card.url}
                  initial="rest"
                  whileHover="hover"
                  animate="rest"
                >
                  <CardImageContainer imageSrc={card.picture}>
                    <CardHoverOverlay
                      variants={{
                        hover: {
                          opacity: 1,
                          height: "auto",
                        },
                        rest: {
                          opacity: 0,
                          height: 0,
                        },
                      }}
                      transition={{ duration: 0.3 }}
                    >
                      {isPlaying ? (
                        <PrimaryAction onClick={() => soundPause()}>
                          <PauseIcon />
                        </PrimaryAction>
                      ) : (
                        <PrimaryAction onClick={() => soundPlay(card.preview)}>
                          <PlayIcon />
                        </PrimaryAction>
                      )}
                    </CardHoverOverlay>
                  </CardImageContainer>
                  <CardText>
                    <CardTitle>{card.title}</CardTitle>
                    <CardContent>{card.name}</CardContent>
                    <CardDurationContainer>
                      <CardDuration>
                        <ClockIcon />
                        {(card.duration / 60).toFixed(2)}
                      </CardDuration>
                    </CardDurationContainer>
                  </CardText>
                </Card>
              </CardContainer>
            ))}
          </TabContent>
        ))}
      </ContentWithPaddingXl>
      <DecoratorBlob1 />
      <DecoratorBlob2 />
      <DecoratorBlob3 />
      <DecoratorBlob4 />
      <DecoratorBlob5 />
      <DecoratorBlob6 />
      <DecoratorBlob7 />
      <DecoratorBlob8 />
    </Container>
  );
};
