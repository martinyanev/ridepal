import React, { useEffect, useState } from "react";
import { BASE_URL } from "helpers/BaseURL";
import SingleTrack from "../Travel/SingleTrack";
import tw from "twin.macro";
import Player from "../Travel/Player";
import { Link } from "react-router-dom";

const Heading = tw.div`mt-0 pt-0 font-body font-black text-5xl text-center min-h-8`;
const Container = tw.div`w-full  mt-0 pt-0 h-2/4 overflow-y-auto`;
const TrackDiv = tw.div`grid grid-cols-1 sm:grid-cols-2  md:grid-cols-4 gap-5 overflow-y-auto h-96`;



const SinglePlaylist = ({ name }) => {
  const [playlist, setPlaylist] = useState([]);
  const [nameOfPlaylist, setNameOfPlaylist] = useState("");
  const [songInPlayer, setSongInPlayer] = useState({
    id: null,
    title: "",
    preview: "",
  });

  useEffect(() => {
    const token = localStorage.getItem("tokenFinal");

    fetch(`${BASE_URL}/playlists/name?playlistName=${name}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setPlaylist(data);
        setNameOfPlaylist(name);
        setSongInPlayer({
          id: 0,
          title: data[0].title,
          preview: data[0].preview,
        });
      });
  }, [name]);

  const handleNext = (id) => {
    if (playlist[id + 1]) {
      setSongInPlayer({
        id: id + 1,
        title: playlist[id + 1].title,
        preview: playlist[id + 1].preview,
      });
    } else {
      setSongInPlayer({
        id: 0,
        title: playlist[0].title,
        preview: playlist[0].preview,
      });
    }
  };

  const handlePrev = (id) => {
    if (id === 0) {
      setSongInPlayer({
        id: playlist.length - 1,
        title: playlist[playlist.length - 1].title,
        preview: playlist[playlist.length - 1].preview,
      });
    } else {
      setSongInPlayer({
        id: id - 1,
        title: playlist[id - 1].title,
        preview: playlist[id - 1].preview,
      });
    }
  };

  const handleAddSong = (id, title, preview) => {
    console.log(id);
    setSongInPlayer({
      id: id,
      title,
      preview,
    });
  };

  return (
    <>
      <Container>
        <Heading>
          {nameOfPlaylist}
        </Heading>

        <TrackDiv>
          {playlist.map((el, index) => (
            <SingleTrack
              key={el.id}
              idInArray={index}
              track_id={el.track_id}
              title={el.title}
              duration={el.duration}
              rank={el.rank}
              preview={el.preview}
              picture={el.picture}
              artist_id={el.artist_id}
              genre_id={el.genre_id}
              album_id={el.album_id}
              handleAddSong={handleAddSong}
            />
          ))}
        </TrackDiv>
        <hr />
      </Container>
      <Player
        songInPlayer={songInPlayer}
        handleNext={handleNext}
        handlePrev={handlePrev}
      />
    </>
  );
};

export default SinglePlaylist;
