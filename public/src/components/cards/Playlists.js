import React, { useState, useEffect, useContext } from "react";
import Slider from "react-slick";
import tw from "twin.macro";
import styled from "styled-components";
import { SectionHeading } from "components/misc/Headings";
import { PrimaryButton as PrimaryButtonBase } from "components/misc/Buttons";
import { ReactComponent as LocationIcon } from "feather-icons/dist/icons/map-pin.svg";
import { ReactComponent as ClockIcon } from "feather-icons/dist/icons/clock.svg";
import { ReactComponent as ChevronLeftIcon } from "feather-icons/dist/icons/chevron-left.svg";
import { ReactComponent as ChevronRightIcon } from "feather-icons/dist/icons/chevron-right.svg";
import AuthContext, { getToken } from "../../providers/authContext";
import Dialog from '@material-ui/core/Dialog';
import SinglePlaylist from './SinglePlaylist';

const Container = tw.div`relative font-body`;
const Content = tw.div`max-w-screen-xl mx-auto py-16 lg:py-20`;
const HeadingTitle = tw.h2`text-2xl sm:text-6xl font-body tracking-wide text-center mr-10`;
const HeadingWithControl = tw.div`flex flex-col items-center sm:items-stretch sm:flex-row justify-center`;
const Heading = tw(SectionHeading)``;
const Controls = tw.div`flex items-center`;
const ControlButton = styled(PrimaryButtonBase)`
  ${tw`mt-4 sm:mt-0 first:ml-0 ml-6 rounded-full p-2`}
  svg {
    ${tw`w-6 h-6`}
  }
`;
const PrevButton = tw(ControlButton)``;
const NextButton = tw(ControlButton)``;

const CardSlider = styled(Slider)`
  ${tw`mt-16`}
  .slick-track {
    ${tw`flex`}
  }
  .slick-slide {
    ${tw`h-auto flex justify-center mb-1`}
  }
`;

const Card = tw.div`m-12 h-full flex! flex-col sm:border max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl 
relative focus:outline-none min-w-playlist max-w-playlist max-h-team
transition duration-300 
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline 
active:bg-primary-300 transform motion-safe:hover:scale-110
`;

const CardImage = styled.div((props) => [
  `background-image: url("${props.imageSrc}");`,
  tw`w-full h-56 sm:h-64 bg-cover bg-center rounded sm:rounded-none sm:rounded-tl-4xl`,
]);

const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const DurationInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6`}
  }
`;
const Duration = tw.span`ml-2 font-bold`;

const PrimaryButton = tw(
  PrimaryButtonBase
)`mt-auto sm:text-lg rounded-none w-full 
rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6
transition duration-300 
text-gray-100 hocus:bg-primary-700 
hocus:text-gray-200  focus:shadow-outline 
active:bg-pink-700 `;

const SecondaryButton = tw(
  PrimaryButtonBase)`mt-auto sm:text-sm w-full 
bg-red-400
transition duration-300 
text-gray-100 hocus:bg-red-700 
hocus:text-gray-200  focus:shadow-outline 
active:bg-pink-700 `;

export default () => {
  const [sliderRef, setSliderRef] = useState(null);

  const auth = useContext(AuthContext);

  const [userInfo, setUserInfo] = useState({});
  const [playlists, setPlaylists] = useState([]);
  const [launchPlaylist, setLaunchPlaylist] = useState(false);
  const [playlistName, setPlaylistName] = useState(''); 

  const token = getToken();
  const id = auth.user.id;
  
  const deletePlaylist = (playlistId) => {
    fetch(`http://localhost:5555/playlists/${playlistId}`, {
        method: "DELETE",
        headers: { Authorization: `Bearer ${token}` },
      })
        .then((res) => res.json())
        .then((data) => setUserInfo(data));
  }

  const openPopUpPlaylist = (name) => {
    setPlaylistName(name);
    setLaunchPlaylist(true);
  }
    
    useEffect(() => {
      fetch(`http://localhost:5555/users/${+auth.user.id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
        .then((res) => res.json())
        .then((data) => setUserInfo(data));

    }, [id]);
 
    
      useEffect(() => {
        fetch(`http://localhost:5555/playlists/user/${+auth.user.id}`, {
          headers: { Authorization: `Bearer ${token}` },
        })
          .then((res) => res.json())
          .then((data) => setPlaylists(data))
          
      }, [playlists]);


  let sliderSettings = {
    arrows: false,
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 2,
        },
      },

      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };
  if (playlists.length === 1) {
    sliderSettings = {
      arrows: false,
      slidesToShow: 1,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 1,
          },
        },

        {
          breakpoint: 900,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    };
  }
  if (playlists.length === 2) {
    sliderSettings = {
      arrows: false,
      slidesToShow: 2,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 2,
          },
        },

        {
          breakpoint: 900,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    };
  }

  return (
    <>
      <Dialog
        open={launchPlaylist}
        fullWidth
        maxWidth="lg"
        onClose={() => setLaunchPlaylist(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <SinglePlaylist name={playlistName} />
      </Dialog>
      {playlists.length > 0 ? (
        <Container>
          <Content>
            <HeadingWithControl>
              <HeadingTitle>Playlists</HeadingTitle>
              <Controls>
                <PrevButton onClick={sliderRef?.slickPrev}>
                  <ChevronLeftIcon />
                </PrevButton>
                <NextButton onClick={sliderRef?.slickNext}>
                  <ChevronRightIcon />
                </NextButton>
              </Controls>
            </HeadingWithControl>
            <CardSlider ref={setSliderRef} {...sliderSettings}>
              {playlists.map((playlist, index) => (
                <Card key={index}>
                  <CardImage
                    imageSrc={playlist.picture==='0.jpg'? 'http://localhost:5555/avatars/notess.png' : playlist.picture}
                  />
                  <TextInfo>
                    <TitleReviewContainer>
                      <Title>{playlist.title}</Title>
                      <DurationInfo>
                        <ClockIcon />
                        <Duration>{(playlist.duration/60).toFixed(2)}</Duration>
                      </DurationInfo>
                    </TitleReviewContainer>
                  </TextInfo>
                  <SecondaryButton onClick={() => deletePlaylist(playlist.id)}>DELETE</SecondaryButton>
                  <PrimaryButton onClick={() => openPopUpPlaylist(playlist.title)}>LISTEN</PrimaryButton>
                </Card>
              ))}
            </CardSlider>
          </Content>
        </Container>
      ) : (
        <Container tw="max-h-team">
          <Content>
            <HeadingWithControl tw="justify-center">
              <Heading>No Playlists Yet</Heading>
            </HeadingWithControl>
          </Content>
        </Container>
      )}
    </>
  );
};
