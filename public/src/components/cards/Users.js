import React, { useState, useEffect, useContext } from "react";
import Slider from "react-slick";
import tw from "twin.macro";
import styled from "styled-components";
import { PrimaryButton as PrimaryButtonBase } from "components/misc/Buttons";
import { ReactComponent as LocationIcon } from "feather-icons/dist/icons/slash.svg";
import { ReactComponent as UserIcon } from "feather-icons/dist/icons/user.svg";
import { ReactComponent as ClockIcon } from "feather-icons/dist/icons/clock.svg";
import { ReactComponent as ChevronLeftIcon } from "feather-icons/dist/icons/chevron-left.svg";
import { ReactComponent as ChevronRightIcon } from "feather-icons/dist/icons/chevron-right.svg";
import AuthContext, { getToken } from "../../providers/authContext";
import SearchBar from "components/SearchBar/SearchBar";

const Container = tw.div`relative grid items-center justify-center font-body bg-primary-100`;
const Content = tw.div`max-w-screen-xl mx-auto py-16 lg:py-20 md:justify-center md:items-center sm:justify-center sm:items-center`;
const HeadingTitle = tw.h2`text-2xl sm:text-6xl justify-center items-center font-body tracking-wide text-center`;
const HeadingTitleNoUsersFound = tw.h2`text-2xl sm:text-6xl mt-48 mb-48 h-full justify-center items-center font-body tracking-wide text-center`;
const HeadingWithControl = tw.div`flex flex-col justify-center items-center sm:items-stretch sm:flex-row justify-center`;
const Controls = tw.div`flex items-center ml-10`;
const ControlButton = styled(PrimaryButtonBase)`
  ${tw`mt-4 sm:mt-0 first:ml-0 ml-6 rounded-full p-2`}
  svg {
    ${tw`w-6 h-6`}
  }
`;
const PrevButton = tw(ControlButton)``;
const NextButton = tw(ControlButton)``;

const CardSlider = styled(Slider)`
  ${tw`mt-16 items-center justify-center`}
  .slick-track {
    ${tw`flex items-center justify-center`}
  }
  .slick-slide {
    ${tw`h-auto flex items-center justify-center mb-1`}
  }
`;
const Card = tw.div`m-12 h-full flex! flex-col border-none max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl 
relative min-w-playlist max-w-playlist max-h-team sm:justify-center sm:items-center
transition duration-300 
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline hocus:shadow-lg
 
active:bg-teal-400 transform motion-safe:hover:scale-110
`;

const CardImage = styled.div((props) => [
  `background-image: url("${props.imageSrc}");`,
  tw`w-full h-56 sm:h-64 bg-cover bg-no-repeat bg-top min-h-genres rounded sm:rounded-none sm:rounded-tl-4xl`,
]);

const TextInfo = tw.div`p-2 justify-center items-center `;
const TitleReviewContainer = tw.div`flex-row  min-w-full justify-between 
items-center sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl text-center font-bold`;

const DurationInfo = styled.div`
  ${tw`flex items-center justify-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 `}
  }
`;
const Duration = tw.span`ml-2 font-bold`;

const SecondaryInfoContainer = tw.div`flex flex-col justify-center items-center 
text-right sm:flex-row mt-2 sm:mt-4 min-w-full`;
const IconWithText = tw.div`flex items-center m-3 mr-6 my-2 sm:my-0`;
const IconContainer = styled.div`
  ${tw`inline-block justify-center items-center rounded-full p-2 bg-gray-700 text-gray-100`}
  svg {
    ${tw`w-3 h-3`}
  }
`;
const Text = tw.div`ml-2 text-sm font-semibold`;

const PrimaryButton = tw(
  PrimaryButtonBase
)`mt-auto sm:text-lg rounded-none w-full 
rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6
transition duration-300 
text-gray-100 hocus:bg-primary-700 
hocus:text-gray-200  focus:shadow-outline 
active:bg-pink-700 `;
const SecondaryButton = tw(PrimaryButtonBase)`mt-auto sm:text-sm w-full 
bg-red-400
transition duration-300 
text-gray-100 hocus:bg-red-700 
hocus:text-gray-200  focus:shadow-outline 
active:bg-pink-700 `;

export default () => {
  const [sliderRef, setSliderRef] = useState(null);

  const auth = useContext(AuthContext);

  const [userInfo, setUserInfo] = useState({});
  const [users, setUsers] = useState([]);
  const [userRole, setUserRole] = useState(false);
  const [banned, setBanned] = useState(false);
  
  const token = getToken();
 
    const id = auth.user.id;
    
    useEffect(() => {
      fetch(`http://localhost:5555/users/${+auth.user.id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
        .then((res) => res.json())
        .then((data) => setUserInfo(data));

      fetch(`http://localhost:5555/users`, {
        headers: { Authorization: `Bearer ${token}` },
      })
        .then((res) => res.json())
        .then((data) => setUsers(data));
    }, [id, userRole, banned]);


  const promoteUser = (id) => {
    fetch(`http://localhost:5555/users/${id}`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUserRole(!userRole);
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const demoteUser = (id) => {
    fetch(`http://localhost:5555/users/${id}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUserRole(!userRole);
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const banUser = (id) => {
    fetch(`http://localhost:5555/users/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setBanned(!banned);
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const unbanUser = (id) => {
    fetch(`http://localhost:5555/users/${id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setBanned(!banned);
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };


  let sliderSettings = {
    arrows: false,
    slidesToShow: 3,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 2,
        },
      },

      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  if (users.length === 2) {
    sliderSettings = {
      arrows: false,
      slidesToShow: 2,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 2,
          },
        },

        {
          breakpoint: 900,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    };
  }

  if (users.length === 1) {
    sliderSettings = {
      arrows: false,
      slidesToShow: 1,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 1,
          },
        },

        {
          breakpoint: 900,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    };
  }

  return (
    <>
    {userInfo.role==='0'? <></>:
    <Container>
    <Content>
      <HeadingWithControl>
        <HeadingTitle>Users</HeadingTitle>
        <Controls>
          <PrevButton onClick={sliderRef?.slickPrev}>
            <ChevronLeftIcon />
          </PrevButton>
          <NextButton onClick={sliderRef?.slickNext}>
            <ChevronRightIcon />
          </NextButton>
        </Controls>
      </HeadingWithControl>
      <SearchBar setUsers={setUsers} users={users}>
        Search
      </SearchBar>
      {users.length > 0 && userInfo.role === "1" ? (
        <CardSlider ref={setSliderRef} {...sliderSettings}>
        {users.map((user, index) => (
          <Card key={index}>
            <CardImage
              imageSrc={
                user.avatarUrl === null
                  ? `http://localhost:5555/avatars/defaultphoto.png`
                  : `http://localhost:5555/avatars/${user.avatarUrl}`
              }
            />
            <TextInfo>
              <TitleReviewContainer>
                <Title>{user.username}</Title>
                <DurationInfo>
                  <ClockIcon />
                  <Duration>{user.email}</Duration>
                </DurationInfo>
              </TitleReviewContainer>
              <SecondaryInfoContainer>
                <IconWithText>
                  <IconContainer>
                    <LocationIcon />
                  </IconContainer>
                  <Text>{user.isBanned === 0 ? "Active" : "Banned"}</Text>
                </IconWithText>
                <IconWithText>
                  <IconContainer>
                    <UserIcon />
                  </IconContainer>
                  <Text>
                    {user.role === "0" ? "Basic User" : "Admin"}
                  </Text>
                </IconWithText>
              </SecondaryInfoContainer>
            </TextInfo>
            {user.isBanned === 1 ? (
              <SecondaryButton
                tw="bg-green-500 hocus:bg-green-700"
                onClick={() => unbanUser(user.id)}
              >
                UNBAN
              </SecondaryButton>
            ) : (
              <SecondaryButton onClick={() => banUser(user.id)}>
                BAN
              </SecondaryButton>
            )}

            {user.role === "1" ? (
              <PrimaryButton
                tw="bg-red-300 hocus:bg-red-700"
                onClick={() => demoteUser(user.id)}
              >
                DEMOTE
              </PrimaryButton>
            ) : (
              <PrimaryButton onClick={() => promoteUser(user.id)}>
                PROMOTE
              </PrimaryButton>
            )}
          </Card>
        ))}
      </CardSlider>
      ) : (
        <>
        <HeadingTitleNoUsersFound>No users found!</HeadingTitleNoUsersFound>
      </>
      )}
      
    </Content>
  </Container>
    }
        
    </>
  );
};
