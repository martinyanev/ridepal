import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import Header, {
  NavLink,
  LogoLink,
  NavToggle,
  DesktopNavLinks,
} from "../headers/light.js";
import AnimationComponent from "helpers/AnimationComponent.js";
import { Link } from "react-router-dom";
import { getToken } from "providers/authContext.js";

const StyledHeader = styled(Header)`
  ${tw` max-w-none w-full`}
  ${DesktopNavLinks} ${NavLink}, ${LogoLink} {
    ${tw`text-gray-100 hover:border-primary-500 w-3/6 hover:text-gray-300`}
  }
  ${NavToggle}.closed {
    ${tw`text-gray-100 hover:text-primary-500`}
  }
`;

const Container = styled.div`
  ${tw`relative -mx-8 -mt-8 bg-center bg-cover  h-screen min-h-144 bg-bg`}
`;

const HeroContainer = tw.div`z-20 relative px-6 sm:px-8 mx-auto h-full flex flex-col`;
const Content = tw.div`px-4 flex flex-1 flex-col justify-center items-center`;

const Heading = styled.h1`
  ${tw`text-3xl text-center sm:text-4xl lg:text-5xl xl:text-6xl 
  font-body font-black text-gray-100 leading-snug -mt-24 sm:mt-0`}
  span {
    ${tw`inline-block mt-2`}
  }
`;

const PrimaryAction = tw.button`
rounded-full  font-body text-xl px-8 py-3 my-10 bg-gray-100 
font-bold shadow transition duration-300 
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline 
active:bg-pink-700 transform motion-safe:hover:scale-110`;

export default () => {
  return (
    <Container>
      <HeroContainer>
        <StyledHeader />
        <Content>
          <AnimationComponent>
            <Heading>
              Browse our <span tw="text-primary-500">featured</span>
              <br />
              songs
            </Heading>
          </AnimationComponent>
          <AnimationComponent>
            {getToken() && (
              <Link to="/travel">
                <PrimaryAction>GENERATE PLAYLIST</PrimaryAction>
              </Link>
            )}
          </AnimationComponent>
        </Content>
      </HeroContainer>
    </Container>
  );
};
