import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import Header, {
  NavLink,
  LogoLink,
  NavToggle,
  DesktopNavLinks,
} from "../headers/light.js";
import AnimationComponent from "helpers/AnimationComponent.js";
import NormalUser from "components/ProfileComponent.js/ProfileComponent.js";

const StyledHeader = styled(Header)`
  ${tw` max-w-none w-full`}
  ${DesktopNavLinks} ${NavLink}, ${LogoLink} {
    ${tw`text-gray-100 hover:border-primary-500 w-3/6 hover:text-gray-300`}
  }
  ${NavToggle}.closed {
    ${tw`text-gray-100 hover:text-primary-500`}
  }
`;

const Container = styled.div`
  ${tw`relative -mx-8 -mt-8 bg-center bg-cover h-screen min-h-144 bg-bg`}
`;

const HeroContainer = tw.div`z-20 relative px-6 sm:px-8 mx-auto h-full flex flex-col`;
const Content = tw.div`px-4 flex flex-1 flex-col justify-center items-center`;

export default () => {
  return (
    <Container>
      <HeroContainer>
        <StyledHeader />
        <Content>
          <AnimationComponent>
            <NormalUser />
          </AnimationComponent>
        </Content>
      </HeroContainer>
    </Container>
  );
};
