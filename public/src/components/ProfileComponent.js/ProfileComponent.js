import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { useContext, useEffect, useState } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import AuthContext, { getToken } from "../../providers/authContext";
import UploadProfilePicture from "./UploadProfilePicture";

const useStyles = makeStyles({
  avatar: {
    height: "100%",
    maxWidth: "100%",
    position: "relative",
    background:
      "linear-gradient(to bottom, rgba(255, 146, 0, 0.5), rgba(0, 0, 0, 0.5))",
    display: "grid",
  },
  root: {
    height: "100%",
    display: "grid",
    position: "relative",
    justifyItems: "center",
    alignContent: "center",
  },
  card: {
    borderRadius: "30px",
    display: "grid",
    margin: 0,
    height: "580px",
    maxWidth: "400px",
    position: "relative",
    background:
      "linear-gradient(to bottom, rgba(0,128,128 ,1 ), rgba(0, 0, 0, 0.5))",
  },
  image: {
    backgroundSize: "cover",
    height: "410px",
    width: "1300px",
  },
  loading: {
    width: "100%",
  },
  input: {
    display: "none",
  },

  textPrimary: {
    fontFamily: "Orbitron",
    fontSize: "1.9rem",
    color: "#fff",
    fontWeight: "900",
  },
  textSecondary: {
    fontFamily: "Orbitron",
    fontSize: "1rem",
    color: "teal",
    fontWeight: "900",
  },
  books: {
    display: "grid",
    position: "relative",
  },
  button: {
    display: "grid",
    position: "relative",
  },
  buttonInside: {
    fontSize: "2.5rem",
    backgroundColor: "rgba(0,0,0,0.4)",
    marginTop: 10,
    width: "700px",
    fontFamily: "Orbitron",
    fontWeight: "900",
    color: "#fff",
  },
});

function NormalUser(props) {
  const classes = useStyles();
  const auth = useContext(AuthContext);
  const [userInfo, setUserInfo] = useState({});
  const id = auth.user.id;

  const token = getToken();
  useEffect(() => {
    fetch(`http://localhost:5555/users/${+auth.user.id}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => res.json())
      .then((data) => setUserInfo(data));
  }, [id]);

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardActionArea>
          <CardMedia
            className={classes.image}
            component="img"
            alt="Contemplative Reptile"
            height="100"
            image={
              userInfo.avatarUrl !== null
                ? `http://localhost:5555/avatars/${userInfo.avatarUrl}`
                : `http://localhost:5555/avatars/defaultphoto.png`
            }
            title="Profile picture"
          />
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              className={classes.textPrimary}
            >
              {auth.user.username}
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              component="p"
              className={classes.textSecondary}
            >
              {auth.user.email}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions className={classes.button}>
          <UploadProfilePicture className={classes.button} />
        </CardActions>
      </Card>
    </div>
  );
}

export default NormalUser;
