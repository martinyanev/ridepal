import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { ReactComponent as SearchIcon } from "feather-icons/dist/icons/search.svg";
import { useEffect, useState } from "react";
import tw from "twin.macro";
import { makeStyles, ThemeProvider, createMuiTheme } from "@material-ui/core";
import { teal } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  root: {
    "& label.Mui-focused": {
      color: "teal",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "teal",
    },
    "& .MuiOutlinedInput-root": {
      borderColor: "teal",
    },
    "& fieldset": {
      borderColor: "teal",
    },
    "&:hover fieldset": {
      borderColor: "teal",
    },
    "&.Mui-focused fieldset": {
      borderColor: "teal",
    },
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: -70,
  },

  searchBar: {
    width: "300px",
    backgroundColor: "transparent",
    transition: "0.5s",
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    borderColor: "teal",
  },
  button: {
    display: "inline-block",
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
    fontFamily: "Orbitron",
    fontWeight: "900",
    color: "teal",
    width: "450px",
  },
  smallButton: {
    fontSize: "3rem",
    color: "teal",
  },
}));

const PrimaryAction = tw.button`
rounded-full  font-body text-xl px-8 py-3 my-10 bg-gray-100 
font-bold shadow transition duration-300 min-w-button
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline 
active:bg-pink-700 transform motion-safe:hover:scale-110`;

const SearchBar = ({ setUsers, users }) => {
  const classes = useStyles();
  const [searchParam, setSearchParam] = useState("");
  const [param, setParam] = useState("username");

  useEffect(() => {
    fetch(`http://localhost:5555/users?${param}=${searchParam}&pageSize=16`)
      .then((res) => res.json())
      .then((data) => {
        if (data.msg) {
          setUsers([]);
          return;
        }
        setUsers(data);
      });
  }, [searchParam]);

  const changeParam = (e) => {
    e.preventDefault();
    if (param === "username") {
      setParam("email");
    } else {
      setParam("username");
    }
  };

  const theme = createMuiTheme({
    palette: {
      primary: teal,
    },
  });

  return (
    <>
      <form autoComplete="off">
        <div className={classes.root}>
          <ThemeProvider theme={theme}>
            <TextField
              label="Search"
              className={classes.searchBar}
              id="mui-theme-provider-standard-input"
              onChange={(e) => setSearchParam(e.target.value)}
            />
          </ThemeProvider>
          <Button className={classes.smallButton}>
            <SearchIcon className={classes.smallButton} fontSize="large" />
          </Button>
          <PrimaryAction onClick={changeParam} variant="outlined">
            {param}
          </PrimaryAction>
        </div>
      </form>
    </>
  );
};

export default SearchBar;
