import React from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import { useState, useContext, useEffect } from "react";
import AuthContext, { getToken } from "../../providers/authContext";
import useAnimatedNavToggler from "../../helpers/useAnimatedNavToggler.js";
import logo from "../../images/logo.svg";
import { ReactComponent as MenuIcon } from "feather-icons/dist/icons/menu.svg";
import { ReactComponent as CloseIcon } from "feather-icons/dist/icons/x.svg";
import { Link } from "react-router-dom";
import { Avatar } from "@material-ui/core";

const Header = tw.header`
  flex justify-center items-center
  max-w-screen-xl mx-auto
  px-16
`;

export const NavLinks = tw.div`inline-block`;

export const NavLink = tw.a`
  text-lg my-2 lg:text-sm lg:mx-6 lg:my-0
  font-black tracking-wide transition duration-300 
  pb-1 border-b-2 border-transparent hover:border-primary-500 hocus:text-primary-500
  
`;

export const PrimaryLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-teal-700 text-gray-100
  hocus:bg-teal-500 hocus:text-gray-200 focus:shadow-outline 
  font-body
  border-b-0
`;

export const LogoLink = styled(NavLink)`
  ${tw`flex items-center font-body text-4xl font-black border-b-0 ml-0! transform motion-safe:hover:scale-110`};

  img {
    ${tw`w-10 mr-3`}
  }
`;

export const MobileNavLinksContainer = tw.nav`flex flex-1 items-center justify-between`;
export const NavToggle = tw.button`
  lg:hidden z-20 focus:outline-none hocus:text-primary-500 transition duration-300
`;
export const MobileNavLinks = motion(styled.div`
  ${tw`lg:hidden z-10 fixed top-0 inset-x-0 mx-4 my-6 p-8 text-center rounded-lg text-gray-900 bg-gray-700`}
  ${NavLinks} {
    ${tw`flex flex-col items-center `}
  }
`);

export const DesktopNavLinks = tw.nav`
  hidden lg:flex flex-1 justify-between items-center
  
`;
const PrimaryAction = tw.button`
rounded-full  font-body text-xl px-8 py-3 my-10 bg-gray-100 
font-bold shadow transition duration-300 
bg-primary-700 text-gray-100 hocus:bg-primary-500 
hocus:text-gray-200 focus:outline-none focus:shadow-outline 
active:bg-pink-700 transform motion-safe:hover:scale-110`;

export default ({
  roundedHeaderButton = false,
  logoLink,
  links,
  className,
  collapseBreakpointClass = "lg",
}) => {
  const auth = useContext(AuthContext);

  const [userInfo, setUserInfo] = useState({});

  if (localStorage.length === 1) {
    const id = auth.user.id;

    const token = getToken();
    useEffect(() => {
      fetch(`http://localhost:5555/users/${+auth.user.id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
        .then((res) => res.json())
        .then((data) => setUserInfo(data));
    }, [id]);
  }

  const avatarUrl =
    userInfo.avatarUrl === null
      ? `
    http://localhost:5555/avatars/defaultphoto.png`
      : `http://localhost:5555/avatars/${userInfo.avatarUrl}`;

  const logout = () => {
    localStorage.clear();
    auth.setAuthState({
      user: null,
      isLoggedIn: false,
    });
    window.location.href = "/home";
  };

  const defaultLinks = [
    <NavLinks key={1}>
      {localStorage.length === 1 ? (
        <Link to="/profile">
          <Avatar
            tw="ml-96 opacity-50 transition duration-300 hocus:opacity-100"
            alt="AvatarAlt"
            src={avatarUrl}
          />
        </Link>
      ) : (
        ""
      )}
    </NavLinks>,
    <NavLinks key={2}>
      {localStorage.length === 1 ? (
        <Link to="/home">
          <PrimaryAction onClick={logout}>LOGOUT</PrimaryAction>
        </Link>
      ) : (
        <Link to="/login">
          <PrimaryAction href="/login">SIGN IN</PrimaryAction>
        </Link>
      )}
    </NavLinks>,
  ];

  const { showNavLinks, animation, toggleNavbar } = useAnimatedNavToggler();
  const collapseBreakpointCss =
    collapseBreakPointCssMap[collapseBreakpointClass];

  const defaultLogoLink = (
    <LogoLink href="/">
      <img src={logo} alt="logo" />
      Ride<span tw="text-primary-500">Palm</span>
    </LogoLink>
  );

  logoLink = logoLink || defaultLogoLink;
  links = links || defaultLinks;

  return (
    <Header className={className || "header-light"}>
      <DesktopNavLinks css={collapseBreakpointCss.desktopNavLinks}>
        {logoLink}
        {links}
      </DesktopNavLinks>

      <MobileNavLinksContainer
        css={collapseBreakpointCss.mobileNavLinksContainer}
      >
        {logoLink}
        <MobileNavLinks
          initial={{ x: "150%", display: "none" }}
          animate={animation}
          css={collapseBreakpointCss.mobileNavLinks}
        >
          {links}
        </MobileNavLinks>
        <NavToggle
          onClick={toggleNavbar}
          className={showNavLinks ? "open" : "closed"}
        >
          {showNavLinks ? (
            <CloseIcon tw="w-6 h-6" />
          ) : (
            <MenuIcon tw="w-6 h-6" />
          )}
        </NavToggle>
      </MobileNavLinksContainer>
    </Header>
  );
};

const collapseBreakPointCssMap = {
  sm: {
    mobileNavLinks: tw`sm:hidden`,
    desktopNavLinks: tw`sm:flex`,
    mobileNavLinksContainer: tw`sm:hidden`,
  },
  md: {
    mobileNavLinks: tw`md:hidden`,
    desktopNavLinks: tw`md:flex`,
    mobileNavLinksContainer: tw`md:hidden`,
  },
  lg: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`,
  },
  xl: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`,
  },
};
