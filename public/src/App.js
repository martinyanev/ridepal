import "tailwindcss/dist/base.css";
import "styles/globalStyles.css";
import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import LandingPage from "views/LandingPage";
import AuthContext, { getUser } from "./providers/authContext";
import LoginForm from "components/login/LoginForm";
import Profile from "views/Profile";
import Travel from "components/Travel/Travel";
import TravelPlaylist from "components/Travel/TravelPlaylist";
import ContactUs from "components/features/ContactUs";
import GenrePage from "views/GenrePage";
import GuardedRoute from "helpers/GuardedRoute";

export default function App() {
  const [authValue, setAuthValue] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });

  return (
    <Router>
      <AuthContext.Provider
        value={{ ...authValue, setAuthState: setAuthValue }}
      >
        <Switch>
          <Redirect path="/" exact to="/home" />
          <Route path="/home" component={LandingPage} />
          <Route path="/login" component={LoginForm} />
          <Route path="/contact-us" component={ContactUs} />
          <GuardedRoute
            path="/profile"
            isLoggedIn={authValue.isLoggedIn}
            component={Profile}
          />
          <GuardedRoute
            path="/travel"
            isLoggedIn={authValue.isLoggedIn}
            component={Travel}
          />
          <GuardedRoute
            path="/genres"
            isLoggedIn={authValue.isLoggedIn}
            component={GenrePage}
          />
          <GuardedRoute
            path="/playlist"
            isLoggedIn={authValue.isLoggedIn}
            component={TravelPlaylist}
          />
        </Switch>
      </AuthContext.Provider>
    </Router>
  );
}
