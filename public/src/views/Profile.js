import React from "react";
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import BackgroundWithProfile from "components/Backgrounds/BackgroundWithProfile";
import Playlists from "components/cards/Playlists";
import Users from "components/cards/Users";
import Footer from "components/footer/footer";
import AnimationComponent from "helpers/AnimationComponent";

export default () => (
  <AnimationRevealPage disabled>
    <BackgroundWithProfile />
    <AnimationComponent>
      <Playlists />
      <Users />
      <Footer />
    </AnimationComponent>
  </AnimationRevealPage>
);
