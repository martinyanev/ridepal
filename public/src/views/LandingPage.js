import React from "react";
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import Hero from "components/Backgrounds/BackgroundAsImageWithCenteredContent";
import Team from "components/AboutUs/Team";
import Footer from "components/footer/footer";
import Features from "components/features/Features";

export default () => (
  <AnimationRevealPage>
    <Hero />
    <Features />
    <Team />
    <Footer />
  </AnimationRevealPage>
);
