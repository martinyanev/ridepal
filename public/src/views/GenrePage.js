import React from "react";
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import Footer from "components/footer/footer";
import AnimationComponent from "helpers/AnimationComponent";
import Genres from "components/cards/Genres";
import BackgroundWithGenre from "components/Backgrounds/BackgroundWithGenre";

export default () => (
  <AnimationRevealPage disabled>
    <BackgroundWithGenre />
    <AnimationComponent>
      <Genres />
      <Footer />
    </AnimationComponent>
  </AnimationRevealPage>
);
