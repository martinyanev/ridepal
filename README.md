# RidePal

Final project for Telerik Academy

## Description

RidePalm is an application you can use to browse through music, check featured songs
and create your own personalized playlists based on travel time and genre preferences on the go.

# Important

In order to run the project check the README in the api and public folders.

# Authors

### - ©️Martin Yordanov
### - ©Martin Yanev
